package exeption_handling.custom_exeption;

import utilities.ScannerHelper;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TestPermission {
    public static void main(String[] args) {

        int age = ScannerHelper.getAnAge();

       try{
           if(Permission.isAgeValid(age)) System.out.println("You can get DL");
       }
       catch (Exception e) {
         e.printStackTrace();
       }
       finally {
           if (age < 16) System.out.println("You are only " + age + ".");
       }


        System.out.println("\n==========new task=================\n");

       int day = ScannerHelper.getANumber();

       try{
           System.out.println(Permission.checkIn(day));
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
       finally {
           Date date = new Date();
           SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE");
           System.out.println("Current day is " + dayFormat.format(date));
       }



    }
}
