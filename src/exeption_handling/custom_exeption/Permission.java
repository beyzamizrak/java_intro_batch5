package exeption_handling.custom_exeption;

public class Permission {

    public static boolean isAgeValid(int age){
        if( age > 16) return true;
        else throw new IllegalStateException("Age of " + age + " is restricted!!!");
    }

    public static String checkIn(int numberOfDay){

        if (numberOfDay > 0 && numberOfDay < 6) System.out.println("Check in hours are from 10 AM to 5PM");
        else if (numberOfDay == 6 || numberOfDay == 7) System.out.println("Check in hours are from 10 AM to 3PM");
        throw new IllegalStateException("The input does not represent any day!!!");
    }



}
