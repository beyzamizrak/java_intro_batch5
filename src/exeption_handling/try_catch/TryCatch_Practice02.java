package exeption_handling.try_catch;

import utilities.ScannerHelper;

import java.util.InputMismatchException;

public class TryCatch_Practice02 {
    public static void main(String[] args) {

        /*
        write a program that gets two numbers from user as an int
        Then divide these numbers and print the result
        Print a message saying "End of the program"

         */



        try{
            int number1 = ScannerHelper.getANumber();
            int number2 = ScannerHelper.getANumber();
            System.out.println(number1/number2);
        }
        catch (ArithmeticException | InputMismatchException e){
            e.printStackTrace();
        }

        System.out.println("End of the program");



    }
}
