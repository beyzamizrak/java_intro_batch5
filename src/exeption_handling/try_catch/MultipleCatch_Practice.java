package exeption_handling.try_catch;

public class MultipleCatch_Practice {

    public static void main(String[] args) {

        String name = null;
        int age = 25;

        /*
        Get the first character from the name and print it
        divide the age by 0 and print the result
        Print {name} 's age is {age}.
        null/s age is 25.
         */


        try {
            System.out.println(name.charAt(0));
            System.out.println(age / 0);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
        catch (ArithmeticException e){
            e.printStackTrace();
        }


        System.out.println(name + "'s age is " + age + ".");



    }
}
