package strings;

public class StringConcatenation {
    public static void main(String[] args) {

        String firstName = "Beyza";
        String lastName = "Mizrak";
        String fullName = firstName + lastName; // BeyzaMizrak

        System.out.println(firstName + " " + lastName); // Beyza Mizrak

        System.out.println("\n===================Concatenation with concat() method==========\n");

        String fullName2 = firstName.concat(" ").concat(lastName);

        System.out.println(fullName2);

    }
}
