package strings;

public class PracticeConcatenation {
    public static void main(String[] args) {


        System.out.println("---------------task1---------------\n");

        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";

        String word = wordPart1 + wordPart2 + wordPart3;

        System.out.print(wordPart1 + wordPart2 + wordPart3);


        System.out.println();

        String sentencePart1 = "I can";
        String sentencePart2 = "learn Java";

        String sentence = sentencePart1.concat(" ").concat(sentencePart2);

        System.out.print(sentencePart1.concat(" ").concat(sentencePart2));




    }
}
