package collections;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class _07_Map_Implementations {
    public static void main(String[] args) {

        TreeMap<Integer, String> map = new TreeMap<>();

        map.put(1, "Beyza");
        map.put(1, "Malek");

        System.out.println(map.size()); // 1
        System.out.println(map);

        map.put(2, "Malek");
        System.out.println(map.size()); // 2
        System.out.println(map); // {1=Malek, 2=Malek}

        //map.put(null, "Andrii");
       // map.put(null, "Vlad");

        System.out.println(map);

        map.put(3, "Victoria");
        map.put(4, null);
        map.put(5, null);
        map.put(6, null);
        System.out.println(map); // {null=Vlad, 1=Malek, 2=Malek, 3=Victoria, 4=null, 5=null, 6=null}

        map.put(-5, "Ashraf");
        map.put(0, "Filiz");

        System.out.println(map); // {-5=Ashraf, 0=Filiz, 1=Malek, 2=Malek, 3=Victoria, 4=null, 5=null, 6=null}



    }
}
