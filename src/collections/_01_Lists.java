package collections;

import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class _01_Lists {
    public static void main(String[] args) {

        /*
        They preserve insertion order
        They allow duplicates
        They allow null elements

        Performance
        Memory
        Initial capacity
        Load
        The way they store the data
         */

        LinkedList<String> fruits = new LinkedList<>();

        fruits.add("Apple");
        fruits.offer("Orange");
        fruits.offerFirst("Pineapple");
        System.out.println(fruits.element()); // element just retrieves the first element just like get first
        System.out.println(fruits.getFirst()); // Pineapple
        System.out.println(fruits);

        fruits.clear();

        System.out.println(fruits.poll()); // null
        //System.out.println(fruits.remove()); // NoSuchElementException
    }
}
