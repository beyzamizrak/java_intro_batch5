package collections;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

import java.util.*;

public class _04_SetMethods {

    public static void main(String[] args) {

        Set<String> set = new TreeSet<>();

        set.add("Foo");
        set.add("Bar");
        set.add("bar");
        set.add("foo");
        set.add("abc");
        set.add("xxx");

        System.out.println(set); //[Bar, bar, abc, Foo, foo, xxx]
        System.out.println(set.size()); // 6
        System.out.println(set.isEmpty()); // false
        System.out.println(set.contains("abc")); // true
        set.remove("AAA"); // false
        System.out.println(set.remove("xxx")); // true
        System.out.println(set);
        set.removeIf(e-> e.toLowerCase().startsWith("b"));
        System.out.println(set);
        System.out.println("\n---------------for each----------------\n");
       set.forEach(System.out::println);
        System.out.println("\n---------------for each loop----------------\n");
        for (String s : set) {
            System.out.println(s);
        }
        System.out.println("\n---------------iterator----------------\n");
        Iterator<String> it = set.iterator();
        while(it.hasNext()) System.out.println(it.next());


        set.removeAll(set); // removing all
        set.clear(); // removing all

        System.out.println(set.size()); /// 0
        System.out.println(set); // []


    }
    }

