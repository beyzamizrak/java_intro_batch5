package collections;

import java.util.*;

public class Practice05_Maps {
    public static void main(String[] args) {

        HashMap<String, String> favorites = new HashMap<>();
        favorites.put("Day", "Friday");
        favorites.put("Color", "Black");
        favorites.put("City", "Chicago");
        favorites.put("Car", "Lexus");
        favorites.put("Pet", "Turtle");

// Keys: Day, Color, City, Car, Pet
// Values: Friday, Black, Chicago, Lexus, Turtle

/*
Get keys and print them
[Car, Color, City, Day, Pet]
 */

        Set<String> set = favorites.keySet();

        TreeSet<String> keys = new TreeSet<>(set);

        Collection<String> collection = favorites.values();

        ArrayList<String> values = new ArrayList<>(collection);

        System.out.println(keys);
        System.out.println(values);

        for (String key : keys) {
            System.out.println(key);
            System.out.println(key.hashCode()); // their addresses in the heap
        }

        // print each value with a loop

        for (String value : values) {
            System.out.println(value);
        }

        // print all entries as below
        System.out.println("--------------------------------");
        int i = 1;
        for (Map.Entry<String, String> entry : favorites.entrySet()) {
            System.out.println(i++ + ". key = " + entry.getKey() + " and the value for it = " + entry.getValue());
        }








    }
}
