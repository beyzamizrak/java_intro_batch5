package collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.TreeSet;

public class _02_Sets {
    public static void main(String[] args) {

        /*
       All  they do not keep insertion order
       All No method using index
       All They do not allow duplicates
       Some differences : HashSet allows one null element due to not allowing duplicates
       LinkedHashSet also allows one null, it keeps insertion order but still doesn't allow methods with indexes
       TreeSet is sorted but doesn't keep insertion order, doesn't allow null, also implements NavigableSet and SortedSet
       */

        HashSet<Integer> hashSet = new HashSet<>();
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
        TreeSet<Integer> treeSet = new TreeSet<>();


        System.out.println("\n----------HashSet------------\n");
        hashSet.add(5);
        hashSet.add(5);
        hashSet.add(10);
        hashSet.add(0);
        hashSet.add(null);
        hashSet.add(null);
        hashSet.add(3);
        hashSet.add(25);
        System.out.println(hashSet);//[0, null, 5, 10] there is no insertion order, randomly adds the elements, doesn't allow duplicates, allows one null

        System.out.println("\n----------LinkedHashSet------------\n");
        linkedHashSet.add(5);
        linkedHashSet.add(5);
        linkedHashSet.add(10);
        linkedHashSet.add(0);
        linkedHashSet.add(null);
        linkedHashSet.add(null);

        System.out.println(linkedHashSet);//[5, 10, 0, null],keeps insertion order doesn't allow duplicates, allows one null

        System.out.println("\n---------TreeSet------------\n");

        treeSet.add(5);
        treeSet.add(5);
        treeSet.add(10);
        treeSet.add(0);
        //treeSet.add(null); // NullPointerException
        //treeSet.add(null); // NullPointerException

        System.out.println(treeSet); //[0, 5, 10] null is not allowed, sorted, no duplicates, it implements two more interfaces



    }
}
