package collections;

import java.util.*;

public class _Practice07_ProductPrices {
    public static void main(String[] args) {

        /*
        Iphone -> 1000
        MacBookPro -> 1300
        iMac -> 1500
        Airpods -> 200
        iPad -> 700
         */
        // my way but not entirely correct
      // TreeMap< String , Double> map = new TreeMap<>();
      // map.put("iPhone", 1000.00);
      // map.put("Macbook Pro", 1300.00);
      // map.put("iMac",1500.00 );
      // map.put("Airpods", 200.00);
      // map.put("iPad", 700.00);

      // double mostExp = new TreeSet<>(map.values()).last();

      // System.out.println(map.descendingKeySet().+ " is the most expensive with the price of " + mostExp) ;

        HashMap<String, Double> products = new HashMap<>();
        products.put("iPhone", 1000.0);
        products.put("Macbook Pro", 1300.0);
        products.put("iMac", 1500.0);
        products.put("AirPods", 200.0);
        products.put("iPad", 700.0);

        double maxPrice = new TreeSet<>(products.values()).last();

        for (Map.Entry<String, Double> entry : products.entrySet()) {
            if(entry.getValue() == maxPrice){
                System.out.println(entry.getKey() + " is the most expensive with the price of $" + entry.getValue());
                break;
            }
        }

    }
}
