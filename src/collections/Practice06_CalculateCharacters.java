package collections;

import java.util.*;

public class Practice06_CalculateCharacters {
    public static void main(String[] args) {


        System.out.println("\n---------------TASK1-----------------\n");
    /*
    You are given a String as banana
    print all duplicated letters
    expected
    a
    n

    key values
    b   1
    a   3
    n   2
     */

        String str = "banana";

        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();

        for (Character c : str.toCharArray()) {
            if(!map.containsKey(c)) map.put(c, 1);
            else map.put(c, map.get(c) + 1);
        }
        System.out.println(map);

        /*
        check the entry
        if the entry value > 1 then print the key of the entry
         */

        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if(entry.getValue() > 1 ) System.out.println(entry.getKey());
        }

        System.out.println("\n---------------TASK2-----------------\n");

        System.out.println(getMostCounted(null));
        System.out.println(getMostCounted(""));
        System.out.println(getMostCounted("a"));
        System.out.println(getMostCounted("apple"));
        System.out.println(getMostCounted("banana"));
        System.out.println(getMostCounted("papa"));
        System.out.println(getMostCounted("abcabcabc"));
        System.out.println(getMostCounted("Maps are nice abd useful but hard :"));

    }

    /*
    create a method that takes a String as an argument and it returns the most occurred character in the string
    if there are more than 1 character that is counted the most then return all
    if the string is empty or null then return nothing
     */

    public static Set<Character> getMostCounted(String str){

        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();
        Set<Character> set = new LinkedHashSet<>();

        if (str == null || str.isEmpty()) return set;
        for (Character c : str.toCharArray()) {
            if(map.containsKey(c)) map.put(c, map.get(c) + 1);
            else map.put(c, 1);
        }

        int maxCount = new TreeSet<>(map.values()).last();


        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if(entry.getValue() == maxCount) set.add(entry.getKey());
        }
        return set;

    }


}


