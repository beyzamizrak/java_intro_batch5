package mathClass;

import java.util.Scanner;

public class AbsoluteMethod {
    public static void main(String[] args) {

        System.out.println("Difference between your numbers is =" + Math.abs(10 - 14));
        System.out.println("Difference between your numbers is =" + Math.abs(14 - 10));
        System.out.println("Subtraction is =" + (10 - 14));

        /*

        Write a program asks user two ages
         */

        Scanner userAnswers = new Scanner(System.in);

        System.out.println("Please enter an age");
        int age = userAnswers.nextInt();

        System.out.println("Please enter another age");
        int age2 = userAnswers.nextInt();

        System.out.println("Difference between two numbers is = " + Math.abs(age - age2));

        System.out.println("Biggest age is = " + Math.max(age, age2));

        System.out.println("Youngest age is = " + Math.min(age, age2));






    }
}
