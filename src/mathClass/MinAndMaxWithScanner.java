package mathClass;

import java.util.Scanner;

public class MinAndMaxWithScanner {
    public static void main(String[] args) {

        /*

        Get 3 numbers from user and find their max and print them with messages.

        Output:

        "Hey user give me a number"
        -> input = 10
        "Hey user give me another number"
        -> input = 15
        "Hey user give me the last number"
        Max of your numbers is 20
        Min of your numbers is 10

         */
        Scanner scan = new Scanner(System.in);


        int num1, num2, num3;

        System.out.println("Hey user give me a number");
        num1 = scan.nextInt();

        System.out.println("Hey user give me another number");
        num2 = scan.nextInt();

        System.out.println("Hey user give me the last number");
        num3 = scan.nextInt();

        System.out.println("Max of your numbers is = " + Math.max(Math.max(num1, num2), num3));
        System.out.println("Min of your numbers is = " + Math.min(Math.min(num1, num2), num3));















    }
}
