package beyzas_package.my_practices;

import java.util.Scanner;

public class MyPractice1_NestedIfElseExamples {
    public static void main(String[] args) {

        System.out.println("\n------------TASK1--------------\n");
        int myInt = 45;

        /*
        if the number is between 0-24 print "first quarter"
        if the number is between 25-49 print "second quarter"
        if the number is between 50-74 print "third quarter"

         */

        if (myInt >= 0 && myInt <= 24) System.out.println("first quarter");
        else if (myInt >= 25 && myInt <= 49) System.out.println("second quarter");
        else if (myInt >= 50 && myInt <= 74) System.out.println("third quarter");


        /*
         if the number is between -50 / -1 print "number is in negative part"
         if the number is between 0 print "number is zero"
         if the number is between 1 / 50 print "number is in positive part"
         */
        int number = -46;

        if(number >= -50 && number < 0){
            System.out.println("number is in negative part");
        }
        else if (number == 0){
            System.out.println("number is zero");
        }
        else if (number > 0 && number <=50){
            System.out.println("number is in positive part");
        }

        int random = (int)(Math.random() * 51 ) + 0;

        if ( random >= 10 && random <= 25) {
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }

        System.out.println((random >= 10 && random <= 25) ? "true" : "false" );

        System.out.println((random >= 10 && random <= 25));

        System.out.println("\n------------TASK2-------------\n");


        int myRandom = (int)(Math.random() * 100) + 1;

        if (myRandom >=51 && myRandom <=100){
            System.out.println(myRandom + " is in the 1st half");
            if (myRandom <= 25){
                System.out.println(myRandom + " is in the 1st quarter");
            }
            else{
                System.out.println(myRandom + " is in the 2nd quarter");
            }
        }
        else {

            System.out.println(myRandom + " is in the 2nd half");
            if (myRandom <= 75) {
                System.out.println(myRandom + " is in the 3rd quarter");
            } else {
                    System.out.println(myRandom + " is in the 4th quarter");
            }
        }

        System.out.println("\n------------TASK3-------------\n");
    /*
    // Scanner//
    Is it raining (true/false)
    Do you have umbrella? (true/false)
    Are you hungry? (true/false)

    // if elses //
    it is raining -> it is raining outside
        if you have umbrella -> you can go outside
        otherwise -> you stay home

    it is not raining -> it is not raining outside
    if you are hungry -> you go to chick-fil-a
    otherwise -> you are riding a bike
    */



        Scanner input = new Scanner(System.in);

        System.out.println("Is it raining(true/false)?");
        boolean isRaining = input.nextBoolean();

        System.out.println("Do you have umbrella(true/false)");
        boolean hasUmbrella = input.nextBoolean();

        System.out.println("Are you hungry(true/false)?");
        boolean isHungry = input.nextBoolean();

        if (isRaining) {
            System.out.println("it is raining outside");
            if(hasUmbrella){
                System.out.println("you can go outside");
            }
            else{
                System.out.println("you stay home");
            }
        }
        else {
            System.out.println("it is not raining outside");
            if(isHungry){
                System.out.println("you go to chick-fil-a");
            }
            else{
                System.out.println("you are riding a bike");
            }
        }




















    }
}
