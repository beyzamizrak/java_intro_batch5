package beyzas_package.my_practices;

import java.util.Scanner;

public class Mock2_Practice_Project04 {
    public static void main(String[] args) {

        // Project04 Task1

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a string!");
        String str = input.nextLine();

        if (str.length() < 8) System.out.println("This string does not have 8 characters");
        else{
            String strNew = str.substring(str.length()-4) + str.substring(4, str.length()-4) + str.substring(0, 4);
            System.out.println(strNew);
        }

        // Project04 Task2

        System.out.println("Please enter a sentence!");
        String sentence = input.nextLine();


        if (!sentence.contains(" ")) System.out.println("This sentence does not have 2 or more words to swap");
        else{
            String newSentence = sentence.substring(sentence.lastIndexOf(" ") + 1) +
                    sentence.substring(sentence.indexOf(" "), sentence.lastIndexOf(" ") + 1) +
                    sentence.substring(0, sentence.indexOf(" "));
            System.out.println(newSentence);

        }

        // Project04 Task3


        String str1 = "These books are so stupid";

        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));
        System.out.println(str2.replace("stupid", "nice").replace("idiot", "nice"));
        System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));




        // Project04 Task4

        System.out.println("Please enter your name!");
        String name = input.nextLine();

        if (name.length() > 2){
            if(name.length() % 2 == 0) System.out.println(name.substring((name.length()/2 -1), (name.length()/2 + 1 )));
            else System.out.println(name.charAt(name.length()/2));
        }
        else System.out.println("Invalid input!!!");

        // Project04 Task5

        System.out.println("Please enter a country");
        String country = input.nextLine();

        if (country.length() < 5) System.out.println("Invalid input!!!");
        else{
            System.out.println(country.substring(2, country.length()-2));
        }

        // Project04 Task6

        System.out.println("Please enter your full address");
        String address = input.nextLine();

        System.out.println(address.replace('a', '*').replace('A', '*').replace('e','#').
                replace('E', '#').replace('i', '+').replace('I', '+').
                replace('U', '$').replace('u', '$').replace('o', '@')
                .replace('O', '@'));



        // Project04 Task7

        int num1 = (int)(Math.random() * (20 - 0 + 1) + 0);
        int num2 = (int)(Math.random() * (20 - 0 + 1) + 0);

        System.out.println(num1);
        System.out.println(num2);

        String answer = "";

        for (int i = Math.min(num1, num2); i <= Math.max(num1, num2); i++) {
            if (i % 5 != 0) answer+= i + " - ";

        }

        System.out.println(answer.substring(0, answer.length()-3));



        // Project04 Task8

        System.out.println("Please enter a sentence");
        String sentence1 = input.nextLine();


        int counter = 0;

        if(!sentence1.contains(" ")) System.out.println("This sentence does not have multiple words");
        else{
            for (int i = 0; i < sentence1.length(); i++) {
                if(sentence1.charAt(i) == ' ') counter++;

            }
            System.out.println("This sentence has " + counter + 1 + " words.");
        }



        // Project04 Task9

        System.out.println("Please enter a positive number");
        int number = input.nextInt();
        input.nextLine();

        for (int i = 1; i <= number ; i++) {
            if(i % 6 == 0) System.out.println("FooBar");
            else if (i % 2 == 0) System.out.println("Foo");
            else if( i % 3 == 0) System.out.println("Bar");
            else System.out.println(i);
        }

        // Project04 Task10

        System.out.println("Please enter a word");
        String word = input.nextLine();

        String reverse = "";

        for (int i = word.length()-1; i >= 0; i--) {
            reverse+= word.charAt(i);
        }

        if (word.length() < 1) System.out.println("This word does not have 1 or more characters");
        else if (word.equals(reverse)) System.out.println("This word is a palindrome");
        else System.out.println("This word is not a palindrome");

        // Project04 Task11

        System.out.println("Please enter a sentence");
        String sentence2 = input.nextLine();

        int counter1 = 0;

        if(sentence2.length() < 1) System.out.println("This sentence does not have any characters");
        else{
            for (int i = 0; i < sentence2.length(); i++) {
                if(sentence2.toLowerCase().charAt(i) == 'a') counter1++;
            }
            System.out.println("This sentence has " + counter1 + " a or A letters.");
        }



    }
}
