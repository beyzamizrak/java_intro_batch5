package projects;

import java.util.Scanner;

public class Project02 {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.println("\n-------------------------TASK1---------------------------\n");


        System.out.print("Hey user, can you give me three numbers!\n");
        int number1 = userInput.nextInt(), number2 = userInput.nextInt(), number3 = userInput.nextInt();

        userInput.nextLine();

        System.out.println("The product of the numbers entered is = " + (number1 * number2 * number3));

        System.out.println("\n-------------------------TASK2---------------------------\n");

        System.out.println("What is your first name?");
        String fname = userInput.nextLine();

        System.out.println("What is your last name?");
        String lname = userInput.nextLine();

        System.out.println("What is your first name?");
        int yearOfBirth = userInput.nextInt();
        userInput.nextLine();

        System.out.println(fname + " " + lname + "'s age is = " + (2022 - yearOfBirth) + "\n");

        System.out.println("\n-------------------------TASK3---------------------------\n");

        System.out.println("What is your full name?");
        String fullName = userInput.nextLine();

        System.out.println("What is your weight?");
        int kgWeight = userInput.nextInt();
        userInput.nextLine();

        System.out.println(fullName + "'s weight is = " + (kgWeight * 2.205) + "\n");

        System.out.println("\n-------------------------TASK4---------------------------\n");

        System.out.println("What is your full name?");
        String name1 = userInput.nextLine();

        System.out.println("What is your age?");
        int age1 = userInput.nextInt();
        userInput.nextLine();

        System.out.println("What is your full name?");
        String name2 = userInput.nextLine();

        System.out.println("What is your age?");
        int age2 = userInput.nextInt();
        userInput.nextLine();

        System.out.println("What is your full name?");
        String name3 = userInput.nextLine();

        System.out.println("What is your age?");
        int age3 = userInput.nextInt();
        userInput.nextLine();

        System.out.println(name1 + "'s age is " + age1 + ".");
        System.out.println(name2 + "'s age is " + age2 + ".");
        System.out.println(name3 + "'s age is " + age3 + ".");
        System.out.println("The average age is " + ((age1 + age2 + age3) / 3) + ".");
        System.out.println("The eldest age is " + Math.max(Math.max(48, 23),34) + ".");
        System.out.println("The youngest age is " + Math.min(Math.min(48 , 23) , 34) + ".");

    }
}
