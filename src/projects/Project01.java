package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("-----------------------Task1-------------------------\n");

        String name = "Beyza";

        System.out.print(name + "\n");

        System.out.println();

        System.out.println("-----------------------Task2-------------------------\n");

        char nameCharacter1 = 'b';
        char nameCharacter2 = 'e';
        char nameCharacter3 = 'y';
        char nameCharacter4 = 'z';
        char nameCharacter5 = 'a';

        System.out.print(" Name Letter 1 is =" + nameCharacter1 + "\n");
        System.out.print(" Name Letter 2 is =" + nameCharacter2 + "\n");
        System.out.print(" Name Letter 3 is =" + nameCharacter3 + "\n");
        System.out.print(" Name Letter 4 is =" + nameCharacter4 + "\n");
        System.out.print(" Name Letter 5 is =" + nameCharacter5 + "\n");

        System.out.println();


        System.out.println("-----------------------Task3-------------------------\n");

        String myFavMovie = "Intouchables";
        String myFavSong = "I am unstoppable by Sia";
        String myFavCity = "Istanbul";
        String myFavActivity = "coding";
        String myFavSnack = "peanut-butter";

        System.out.print("My favorite movie is = " + myFavMovie + "\n");
        System.out.print("My favorite song is = " + myFavSong + "\n");
        System.out.print("My favorite city is = " + myFavCity + "\n");
        System.out.print("My favorite activity is = " + myFavActivity + "\n");
        System.out.print("My favorite snack is = " + myFavSnack + "\n");

        System.out.println();

        System.out.println("-----------------------Task4-------------------------\n");

        int myFavNumber = 3;
        int numberOfStatesIVisited = 10;
        int numberOfCountriesIVisited = 4;

        System.out.print("My favorite number is = " + myFavNumber + "\n");
        System.out.print("My favorite number is = " + numberOfStatesIVisited + "\n");
        System.out.print("My favorite number is = " + numberOfCountriesIVisited + "\n");
        System.out.println();

        System.out.println("-----------------------Task5-------------------------\n");

        boolean amIAtSchoolToday = false;

        System.out.println(" I am at school today = " + amIAtSchoolToday);

        System.out.println();

        System.out.println("-----------------------Task5-------------------------\n");

        boolean isWeatherNiceToday = true;

        System.out.println(" Weather is nice today = " + isWeatherNiceToday);


    }
}

