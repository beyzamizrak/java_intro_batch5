package projects;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n----------------------------TASK1----------------------------\n");

        String s1 = "24", s2 = "5";

        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);

        System.out.println("The sum of " + i1 + " and " + i2 + " = " + (i1 + i2));
        System.out.println("The subtraction of " + i1 + " and " + i2 + " = " + (i1 - i2));
        System.out.println("The division of " + i1 + " and " + i2 + " = " + ((double)i1 / i2));
        System.out.println("The multiplication of " + i1 + " and " + i2 + " = " + (i1 * i2));
        System.out.println("The remainder of " + i1 + " and " + i2 + " = " + (i1 % i2));

        System.out.println("\n----------------------------TASK2----------------------------\n");

        int num1 = (int)(Math.random() * 35) + 1;
        System.out.println(num1);

        if ((num1 == 1) || (num1 % 2 == 0) || (num1 % 3 == 0) || (num1 != 2) || (num1 !=3)){
            System.out.println("THE NUMBER IS NOT A PRIME NUMBER");
        }
        else{
            System.out.println("THE NUMBER IS A PRIME NUMBER");
        }

        System.out.println("\n----------------------------TASK3----------------------------\n");

        int random1 = (int)(Math.random() * 50) + 1, random2 = (int)(Math.random() * 50) + 1,
                random3 = (int)(Math.random() * 50) + 1;

        System.out.println("Random number 1 = " + random1);
        System.out.println("Random number 2 = " + random2);
        System.out.println("Random number 3 = " + random3);

        System.out.println("The smallest number is = " + Math.min(Math.min(random1, random1) , random3));
        System.out.println("The biggest number is = " + Math.max(Math.max(random1, random1) , random3));
        if ((random1 < random2 && random2 < random3) || (random3 < random2 && random2 < random1)){
            System.out.println("The middle number is = " + random2);
        }

        System.out.println("\n----------------------------TASK4----------------------------\n");

        char chr1 = 'a';
        System.out.println(chr1);

        if ((chr1 >= 65 && chr1 <= 90)||(chr1 >= 97 && chr1<= 122)){
            if((chr1 >= 65 && chr1 <= 90)){
                System.out.println("The letter is uppercase");
            }
            else {
                System.out.println("The letter is lowercase");
            }

        }
        else{
            System.out.println("Invalid character detected!!!");
        }


        System.out.println("\n----------------------------TASK5----------------------------\n");

        char c1 = 's';
        System.out.println(c1);

        if ((c1 >= 65 && c1 <= 90)||(c1 >= 97 && c1<= 122)) {
            if((c1 == 97) || (c1 == 101) || (c1 == 105) || (c1 == 111) || (c1 == 117) || (c1 == 65) ||
                    (c1 == 69) || (c1 == 73) || (c1 == 79) || (c1 == 85)) {
                System.out.println("The letter is vowel");
            }
            else {
                System.out.println("The letter is consonant");
            }

        }
        else {
            System.out.println("Invalid character detected!!!");
        }


        System.out.println("\n----------------------------TASK6----------------------------\n");

        char c = '?';
        System.out.println(c);


        if ((c >= 65 && c <= 90)||(c >= 97 && c<= 122) || (c >= 48 && c <= 57)){
            System.out.println("Invalid character detected!!!");
        }
        else{
            System.out.println("Special character is = " + c );
        }

        System.out.println("\n----------------------------TASK7----------------------------\n");

        char chr = '2';
        System.out.println(chr);

        if ((chr >= 65 && chr <= 90) || (chr >= 97 && chr <= 122)){
            System.out.println("Character is a letter");
        }
        else if (chr >= 48 && chr <= 57){
            System.out.println("Character is a digit");
        }
        else {
            System.out.println("Character is a special character");
        }

    }
}
