package projects;

import java.util.Arrays;

public class Project05 {
    public static void main(String[] args) {
        System.out.println("\n-------------------TASK-1---------------\n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);
        System.out.println("\n-------------------TASK-2---------------\n");
        findGreatestAndSmallest(numbers);
        System.out.println("\n-------------------TASK-3---------------\n");
        int[] numbers1 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSecondSmallestWithSort(numbers1);
        System.out.println("\n-------------------TASK-4---------------\n");
        findSecondGreatestAndSmallest(numbers1);
        System.out.println("\n-------------------TASK-5---------------\n");
        String[] str = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(str);
        System.out.println("\n-------------------TASK-6---------------\n");
        String[] str1 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(str1);
    }

    public static void findGreatestAndSmallestWithSort(int[] numbers) {

        Arrays.sort(numbers);

        System.out.println("Smallest = " + numbers[0]);
        System.out.println("Greatest = " + numbers[numbers.length - 1]);

    }

    public static void findGreatestAndSmallest(int[] numbers1) {

        int max = Integer.MIN_VALUE; // -1231232324
        int min = Integer.MAX_VALUE;

        for (int i : numbers1) {
            if (max < i) {
                max = i;
            }
            if (min > i) {
                min = i;
            }
        }

        if (numbers1.length > 0) {
            System.out.println("Smallest = " + min);
            System.out.println("Greatest = " + max);
        } else {
            System.out.println("Array is empty!");
        }
    }

    public static void findSecondGreatestAndSecondSmallestWithSort(int[] numbers) {

        Arrays.sort(numbers);

        int min = numbers[0], max = numbers[numbers.length - 1];
        int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;

        for (int n : numbers) {
            if (secondMax < n && n < max) {
                secondMax = n;
            }
            if (secondMin > n && n > min) {
                secondMin = n;
            }
        }
        if (numbers.length > 0) {
            System.out.println("Second Smallest = " + secondMin);
            System.out.println("Second Greatest = " + secondMax);
        } else System.out.println("This is an empty array!");

    }


    public static void  findSecondGreatestAndSmallest(int[] numbers){

        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;
        for (int number : numbers) {
            if(max < number){
                max = number;
            }
            if(min > number){
                min = number;
            }
        }

        int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;

        for (int number : numbers) {
            if (secondMax < number && number != max){
                secondMax = number;
            }
            if (secondMin > number && number != min){
                secondMin = number;
            }
        }

        if(numbers.length > 0){
            System.out.println("Smallest = " + secondMin);
            System.out.println("Greatest = " + secondMax);
        }else System.out.println("This array is empty!");
    }

    public static void  findDuplicatedElementsInAnArray(String[] str){

        String dup = "";

        for (int i = 0; i < str.length - 1; i++) {
            for (int j = i + 1; j < str.length; j++) {
                // if we already checked break it
                if (dup.contains(str[i] + "")) break;

                // if we didn't check it add it in dup container
                if (str[i] == str[j]){
                    System.out.println(str[i]);
                    dup += str[j];
                }
            }
        }
    }

    public static void findMostRepeatedElementInAnArray(String[] str){

        String mostRepeated = "";
        int freq = 0;
        int count = 0;
        for (int i = 0, j = i + 1; i < str.length -1; i++, j++) {
            if(str[j].equals(str[i])){
                count++;
            }
            if(count >= freq) {
                mostRepeated = str[i];
                freq = count;
            }
        }
        System.out.println(mostRepeated);

    }
}

