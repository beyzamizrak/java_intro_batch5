package projects;

import utilities.RandomNumberGenerator;
import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {


        System.out.println("\n_________________________________TASK_1_________________________________\n");

        String MyStr = ScannerHelper.getAString();

        if (MyStr.length() < 8) System.out.println("This String does not have 8 characters");
        else{
            System.out.println(MyStr.substring(MyStr.length()-4 ) + MyStr.substring(4, MyStr.length()- 4) + MyStr.substring(0, 4));
        }

        System.out.println("\n_________________________________TASK_2_________________________________\n");

        String sentence = ScannerHelper.getASentence();


        if (!sentence.contains(" ")){
            System.out.println("This sentence does not have 2 or more words to swap");
        }
        else{
            System.out.println(sentence.substring(sentence.lastIndexOf(' ') + 1) +
                    sentence.substring(sentence.indexOf(' '), sentence.lastIndexOf(' ') + 1) +
                    sentence.substring(0, sentence.indexOf(' ')));
        }

        System.out.println("\n_________________________________TASK_3_________________________________\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));
        System.out.println(str2.replace("idiot", "nice").replace("idiot", "nice"));
        System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));

        System.out.println("\n_________________________________TASK_4_________________________________\n");

        String name = ScannerHelper.getAName();

        if (name.length() > 2) {
            if (name.length() % 2 == 0) System.out.println(name.substring(name.length()/2 -1, name.length()/2 +1));
            else System.out.println(name.charAt(name.length()/2));
        }
        else System.out.println("Invalid input!!!");

        System.out.println("\n_________________________________TASK_5_________________________________\n");

        String country = ScannerHelper.getACountry();

        if (country.length() > 5){
            System.out.println(country.substring(2, country.length()-2));
        }
        else System.out.println("Invalid input!!!");

        System.out.println("\n_________________________________TASK_6_________________________________\n");

        String address = ScannerHelper.getAnAddress();

        String newAddress = address.replace('a', '*').replace('A', '*').replace('e','#').
                replace('E', '#').replace('i', '+').replace('I', '+').
                replace('U', '$').replace('u', '$').replace('o', '@')
                .replace('O', '@');

        System.out.println(newAddress);



        System.out.println("\n_________________________________TASK_7_________________________________\n");


        int randomNumber1 = RandomNumberGenerator.getARandomNumber(0,25);
        int randomNumber2 = RandomNumberGenerator.getARandomNumber(0,25);

        // I like to print my random numbers so I can see them to verify that my code is working correctly

        System.out.println("Random number 1 is = " + randomNumber1);
        System.out.println("Random number 2 is = " + randomNumber2);

        String answer = "";

        for (int i = Math.min(randomNumber1, randomNumber2); i <= Math.max(randomNumber1, randomNumber2) ; i++) {
            if (i % 5 != 0) answer += i + " - ";

        }

        System.out.println(answer.substring(0, answer.length() -3));

        System.out.println("\n_________________________________TASK_8_________________________________\n");

        String sentence1 = ScannerHelper.getASentence();

        int counter = 0;

        if(!sentence1.contains(" ")) System.out.println("This sentence does not have multiple words.");
        else {
            for (int i = 0; i < sentence1.length(); i++) {
                if(sentence1.charAt(i) == ' ') counter++;
            }
            System.out.println("This sentence has " + (counter + 1) + " words.");
        }

        System.out.println("\n_________________________________TASK_9_________________________________\n");

        int positiveNumber = ScannerHelper.getAPositiveNumber();

        for (int i = 1; i <= positiveNumber ; i++) {
            if (i % 2 == 0 && i % 3 == 0) System.out.println("FooBar");
            else if (i % 2 == 0) System.out.println("Foo");
            else if (i % 3 == 0) System.out.println("Bar");
            else System.out.println(i);
        }


        System.out.println("\n_________________________________TASK_10_________________________________\n");


        String word = ScannerHelper.getAWord();
        String reverse = "";

        for (int i = word.length() -1; i >= 0 ; i--) {
            reverse+= word.charAt(i);
        }
        if (word.length() == 0) System.out.println("This word does not have 1 or more characters");
        else if(word.equals(reverse)) System.out.println("This word is palindrome");
        else System.out.println("This word is not palindrome");


        System.out.println("\n_________________________________TASK_11_________________________________\n");

        String sentence2 = ScannerHelper.getASentence();

        int counter1 = 0;

        if(sentence2.length() < 1) System.out.println("This sentence does not have any characters");
        else {
            for (int i = 0; i < sentence2.length(); i++) {
                if(sentence2.toLowerCase().charAt(i) == 'a')
                    counter1++;

            }
            System.out.println("This sentence has " + counter1 + " a or A letters.");
        }
    }
}
