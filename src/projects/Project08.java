package projects;

import java.util.Arrays;

public class Project08 {

    // TASK 1
    public static int findClosestDistance(int[] numbers){

        if (numbers.length >= 2) {

            Arrays.sort(numbers);
            int min = numbers[numbers.length-1];

            for (int i = 0; i < numbers.length -1; i++) {
                min = Math.min(min, numbers[i+1] - numbers[i]);
            }
            return min;
        }
        else return -1;
    }

    //TASK 2

    public static int findSingleNumber(int[] array) {

        for (int i = 0; i < array.length; i++) {
            int occur = 0;

            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    occur++;
                }
            }
            if (occur == 1) {
                return array[i];
            }
        }
        return -1;
    }

    //TASK 3
    public static char findFirstUniqueCharacter(String str){

        for (int i = 0; i < str.length(); i++) {
            boolean unique = true;
            for (int j = 0; j < str.length(); j++) {
                if( i != j && str.charAt(i) == str.charAt(j)){
                    unique = false;
                    break;
                }
            }
            if(unique)
                return str.charAt(i);
        }
        return ' ';
    }


    // TASk 4
    public static int  findMissingNumber(int[] numbers){
        Arrays.sort(numbers);
        int missing = 0;
        for (int i = 0; i < numbers.length-1; i++) {
            if(Math.abs(numbers[i]-numbers[i+1]) == 2) missing = numbers[i]+1;
        }
        return missing;
    }

    public static void main(String[] args) {
        System.out.println("\n----------------TASK1--------------------\n");

        int[] numbers = {4, 8, 7, 15};
        System.out.println(findClosestDistance(numbers));

        System.out.println("\n----------------TASK2--------------------\n");

        int[] numbers1 = {5, 3, -1, 3, 5, 7, -1};
        System.out.println(findSingleNumber(numbers1));

        System.out.println("\n----------------TASK3--------------------\n");

        String str = "Hello";
        System.out.println(findFirstUniqueCharacter(str));

        System.out.println("\n----------------TASK4--------------------\n");

        int[] numbers3 = {4, 6, 7, 8};

        System.out.println(findMissingNumber(numbers3));
    }

}
