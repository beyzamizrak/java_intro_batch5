package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Project06 {
    public static void main(String[] args) {

        System.out.println("\n-----------------TASK1-----------------\n");
        String[] arr = {"foo", "", " ", "foo bar", "java is fun", " ruby ", "java is more fun"};
        System.out.println(countMultipleWords(arr));

        System.out.println("\n-----------------TASK2-----------------\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));
        System.out.println(removeNegatives(numbers));

        System.out.println("\n-----------------TASK3-----------------\n");

        String password = "";

        System.out.println(validatePassword(password));

        System.out.println("\n-----------------TASK4-----------------\n");
        String email = "beyza@gmail.com";

        System.out.println(validEmail(email));


    }

    public static int countMultipleWords(String[] arr){

        int count = 0;

        for (String s : arr) {
            if(Pattern.matches("[ a-zA-Z]+ [ a-zA-Z]+", s)) count++;
        }
        return count;

    }

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers){

        ArrayList<Integer> newNumbers = new ArrayList<>();
        for (Integer number : numbers) {
            if (number >= 0) newNumbers.add(number);
        }
        return newNumbers;
    }

    public static boolean validatePassword(String password) {
        if(password.matches(".?=*[a-z][A-Z][0-9][!@#$%&*]{8,16}")) return true;
        else return false;
    }

    public static boolean validEmail(String email){

        return email.matches("[\\w]{2,}@[\\w]{2,}\\.[\\w]{2,}");
    }


}
