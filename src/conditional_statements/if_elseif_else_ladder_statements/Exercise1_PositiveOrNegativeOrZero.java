package conditional_statements.if_elseif_else_ladder_statements;

import java.util.Scanner;

public class Exercise1_PositiveOrNegativeOrZero {
    public static void main(String[] args) {
        /*

        Write a program that ask user to enter a number
        if number is more than 0, print positive
        if number is less than 0, print negative
        otherwise print 0
         */

        Scanner inputReader = new Scanner(System.in);

        System.out.println("Hey user, give me a number? ");
        int num = inputReader.nextInt();

        if(num > 0){
            System.out.println("POSITIVE");
        }
         else if(num < 0){
            System.out.println("NEGATIVE");
        }
         else{
            System.out.println("ZERO");
        }
    }
}
