package conditional_statements.turnary_operator;

import java.util.Random;

public class Exercise2_FindMIn {
    public static void main(String[] args) {

        /*
        write a program that generates 2 random numbers between 1 and 10
        find and print the smallest number
        use turnary

         */

        Random random = new Random();


        int min1 = random.nextInt(10) + 1; // 1 to 10
        int min2 = random.nextInt(10) + 1; // 1 to 10

        System.out.println("Random 1 = " + min1);
        System.out.println("Random 2 = " + min2);

        int min = min1< min2 ? min1 : min2;

        System.out.println("The min = " + min);







    }
}
