package conditional_statements.turnary_operator;

public class UnderstandingTurnary {
    public static void main(String[] args) {

        /*

        F = female -> Jane
        M = male    -> John
         */
        char gender = 'f';

        String name = gender == 'F' || gender == 'f' ? "Jane" : "John";


    }
}
