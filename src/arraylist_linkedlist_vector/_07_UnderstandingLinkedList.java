package arraylist_linkedlist_vector;

import java.util.Arrays;
import java.util.LinkedList;

public class _07_UnderstandingLinkedList {
    public static void main(String[] args) {

        LinkedList<Double> numbers = new LinkedList<>(Arrays.asList(10.5, 5.5, 20.0));

        System.out.println(numbers);
        System.out.println(numbers.size());
        System.out.println(numbers.contains(10.5));
        System.out.println(numbers.indexOf(4));
        System.out.println(numbers.get(1));

        System.out.println(numbers.getFirst());
        System.out.println(numbers.getLast());
        System.out.println(numbers.offerFirst(4.5));
        System.out.println(numbers);
        System.out.println(numbers.peek()); // IT GETS THE FIRST ELEMENT
        System.out.println(numbers);
        System.out.println(numbers.pop());
        System.out.println(numbers); // it returns the first element and removes the first element
        System.out.println(numbers.pollFirst()); // it returns the first element and removes the first element
        System.out.println(numbers);
        System.out.println(numbers.pollLast()); // retrieves and the removes the last element
        System.out.println(numbers);
        numbers.push(2.0);
        System.out.println(numbers); // it adds an element to the 0th index like addFirst




    }
}
