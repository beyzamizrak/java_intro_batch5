package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _02_StringArrayList {
    public static void main(String[] args) {

        /*
        Create an ArrayList and store below cities in it
                Chicago
        Berlin
                Miami

        Print the size of the ArrayList
        Print the ArrayList

        EXPECTED:
        3
                [Chicago, Berlin, Miami]

 */

        System.out.println("\n------TASK-1------\n");

        ArrayList<String> cities = new ArrayList<>();

        cities.add("Chicago");
        cities.add("Berlin");
        cities.add("Miami");

        cities.set(2, "Evanston");

        System.out.println(cities.size());
        System.out.println(cities);

        // cities.remove(1);
        cities.remove("Berlin");
        System.out.println(cities);
        System.out.println(cities.size());

        System.out.println(cities.remove("Chicago"));

        cities.add("New York");
        cities.add("Rome");
        cities.add("Gent");

        System.out.println(cities);
        System.out.println(cities.size());

        //cities.clear();
        cities.removeAll(cities);

        System.out.println(cities);
        System.out.println(cities.size());



    }
}
