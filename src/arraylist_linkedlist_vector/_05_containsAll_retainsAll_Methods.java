package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _05_containsAll_retainsAll_Methods {
    public static void main(String[] args) {
        ArrayList<String> countries = new ArrayList<>();

        countries.add("Italy");
        countries.add("USA");
        countries.add("Canada");
        countries.add("Germany");
        countries.add("Spain");
        countries.add("Portugal");
        countries.add("Sweden");

        // check if the countries arrayList contains "Spain"

        System.out.println(countries.contains("Spain"));

        // check of the countries arrayList contains Sweden and Denmark

        System.out.println(countries.contains("Sweden") && countries.contains("Denmark"));

        boolean hasSweden = false, hasDenmark = false;

        for (String country : countries) {
            if(country.equals("Sweden")) hasSweden = true;
            else if (country.equals("Denmark")) hasDenmark = true;

            if(hasDenmark && hasSweden) break;

        }
        System.out.println(hasDenmark && hasSweden);

        // check if teh countries ArrayList contains Sweden, Spain, Germany, Portugal, Italy

        ArrayList<String> europeCountries = new ArrayList<>();

        europeCountries.add("Sweden");
        europeCountries.add("Spain");
        europeCountries.add("Germany");
        europeCountries.add("Portugal");
        europeCountries.add("Italy");

        System.out.println(countries.containsAll(europeCountries));

        countries.retainAll(europeCountries);

        System.out.println(countries);

    }
}
