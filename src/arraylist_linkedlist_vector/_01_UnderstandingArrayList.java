package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_UnderstandingArrayList {
    public static void main(String[] args) {

        // 1. Declaring and Array vs an ArrayList

        System.out.println("\n------------------Declaring array vs Arraylist--------------------\n");

        String[] names1 = new String[3];

        ArrayList<String> names2 = new ArrayList<>(); // capacity = 10 by default but you can change it if u want to

        // 2. Getting the size of the Array vs ArrayList

        System.out.println("\n------------------Size of array vs Arraylist--------------------\n");

        System.out.println("The size of the Array is = " + names1.length); // 3
        System.out.println("The size of the ArrayList is = " + names2.size()); // 0

        // 3. Printing an Array vs an ArrayList

        System.out.println("\n------------------Printing array vs Arraylist--------------------\n");

        System.out.println("The array is = " + Arrays.toString(names1)); // [null, null, null]
        System.out.println("The ArrayList is = " + names2); // []

        // 4. Adding elements to specific indexes

        names1[0] = "Alex";
        names2.add(0, "John"); // names2.add("John");

        System.out.println("The array is = " + Arrays.toString(names1)); // [Alex, null, null]
        System.out.println("The ArrayList is = " + names2); // [John]


        names1[1] = "Ali";
        names1[2] = "Andrii";

        names2.add(1, "Joe");
        names2.add(2, "Jane");

        System.out.println("The array is = " + Arrays.toString(names1)); // [Alex, Ali, Andrii]
        System.out.println("The ArrayList is = " + names2); // [John, Joe, Jane]

        names2.add("Abdallah");
        names2.add("Vlad");
        names2.add("Saeed");
        names2.add("Suzanne");
        names2.add("Hazal");
        names2.add("Yildiz");
        names2.add("Samir");
        System.out.println("The ArrayList is = " + names2);
        System.out.println("The size of the ArrayList = " + names2.size());

        // 5. Updating an existing element in array vs ArrayList

        names1[1] = "Mike";
        names2.set(1, "recep");

        System.out.println("The array is = " + Arrays.toString(names1));
        System.out.println("The ArrayList is = " + names2.set(1, "recep"));















    }
}
