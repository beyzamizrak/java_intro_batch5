package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class Exercise01_countElements {
    public static void main(String[] args) {

        System.out.println("\n------TASK-1-------\n");

        ArrayList<String> colors = new ArrayList<>();
        colors.add("Blue");
        colors.add("Brown");
        colors.add("Pink");
        colors.add("Yellow");
        colors.add("Red");
        colors.add("Purple");

        System.out.println(colors);
        System.out.println(colors.size());

        for (String color : colors) {
            System.out.println(color);

        }

        System.out.println("\n------elements that has length of six-------\n");


        int counter = 0;

        for (String color : colors) {
            if(color.length() == 6){
            counter++;
        }
        }
        System.out.println(counter);

        System.out.println("\n------count the elements that has 'o'-------\n");

        int countO = 0;

        for (String color : colors) {
            if(color.toLowerCase().contains("o")){
                countO++;
            }

        }
        System.out.println(countO);

        int counter0 = 0;

        for (int i = 0; i < colors.size(); i++) {
            if(colors.get(i).contains("o")) counter0++;
        }

        System.out.println(counter0);




    }
}
