package practice.loopsPractice;

public class ForIPractice2 {
    public static void main(String[] args) {


        System.out.println("\nHard coded print statements:\n");

        System.out.println("Number: " + "1");
        System.out.println("Number: " + "2");
        System.out.println("Number: " + "3");
        System.out.println("Number: " + "4");
        System.out.println("Number: " + "5");

        System.out.println("\nFori loop number counter version1:\n");

        for (int i = 1; i < 6; i++) {
            System.out.println("Number: " + i);

        }

        System.out.println("\nFori loop number counter version2:\n");

        for (int i = 0; i < 6; i++) {
            System.out.println("Number: " + (i+1));

        }






    }
}
