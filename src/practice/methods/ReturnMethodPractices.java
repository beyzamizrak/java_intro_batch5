package practice.methods;

public class ReturnMethodPractices {
    public static void main(String[] args) {

        //System.out.println(ReturnMethodPractices.findDifference(6 , 9));

        System.out.println(ReturnMethodPractices.isIncluded("John", "John Doe"));
        System.out.println(ReturnMethodPractices.isIncluded("John Doe", "John"));
        System.out.println(ReturnMethodPractices.isIncluded("John", "Doe"));



    }

    /*
    Create a public static method named as "findDifference" it will take two decimal values and returns
    the positive difference
    Example:
    6, 9 -> 3
    4, 1 -> 3
    99, 23 -> 76
    */

    public static int findDifference(int a, int b){

        return Math.abs(a -b);
    }
    // we can also overload the same method using different type of argument
    public static double findDifference(double double1, double double2){
        return Math.abs(double1 - double2);
    }

    public static boolean isIncluded(String str1, String str2){
        //if (str1.length() > str2.length()) return str1.contains(str2);
        //return str2.contains(str1);

        //alternative
        return str1.contains(str2) || str2.contains(str1);

    }




}
