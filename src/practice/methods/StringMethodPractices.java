package practice.methods;

public class StringMethodPractices {
    public static void main(String[] args) {

    }

    public static boolean isCharExisting(String str, char c, int index){

        return (str.charAt(index) == c);
    }

    public static boolean isCharContained(char chr, String s){

        return s.contains(String.valueOf(chr));
    }

}
