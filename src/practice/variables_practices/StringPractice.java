package practice.variables_practices;

public class StringPractice {
    public static void main(String[] args) {

        String schoolName = "TechGlobal";
        String myName = "Beyza";
        int myAge = 33;
        double myHeight = 6.0;
        boolean isEligibleToDrive = true;

        System.out.println("My school name is " + schoolName + ". My name is" + myName +
                ". I am " + myAge + " years old. My Height is " + myHeight + "." + " I am " +
                "eligible to drive \"" + isEligibleToDrive + "\"" );

        /*
            Java is a high-level\hard, class-based, object-oriented programming language that is
            designed to have as few implementation “dependencies” as possible. It is a general-purpose
            programming language intended to let programmers write once, run anywhere (WORA),
            meaning that compiled Java code can run on all platforms that support Java without the need to recompile.

         */

        System.out.println("\tJava is a high-level\\hard, class-based, object-oriented programming language that is" +
                "\ndesigned to have as few implementation" + "\"" + "dependencies" + "\"" + "as possible. It is a general-purpose" +
                "\nprogramming language intended to let programmers write once, run anywhere (WORA," +
                "\nmeaning that compiled Java code can run on all platforms that support Java without the need to recompile.");








    }
}
