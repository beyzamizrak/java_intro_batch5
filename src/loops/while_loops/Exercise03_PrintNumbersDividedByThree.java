package loops.while_loops;

public class Exercise03_PrintNumbersDividedByThree {
    public static void main(String[] args) {

        // print numbers divided by three between 1 and 100

        int i = 1;

        while (i <= 100){
            if (i % 3 == 0) System.out.println(i);
            i++;
        }
    }
}
