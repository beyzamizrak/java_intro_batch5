package loops.while_loops;

public class Exercise01_PrintOddNumbers {
    public static void main(String[] args) {
        /*
        Print Odd numbers from 1 to 10 both included using while loop
        1
        3
        5
        7
        9

         */
        int start = 1;

        while (start <= 10){
            if (start % 2 == 1) System.out.println(start);
            start++;

        }
    }
}
