package loops.do_while_loops;

public class UnderstandingWhileLoop {
    public static void main(String[] args) {

        System.out.println("\n____________________Do-While Loop________________\n");
        int i = 0;

        do{
            System.out.println(i);
            i++;
        }
        while (i < 5);



        System.out.println("\n____________________While Loop________________\n");
        int j = 0;

        while(j < 5){
            System.out.println(j);
            j++;
        }

        System.out.println("end of the program");
    }

    public static class Exercise02_GetNumberDividedBy5 {
        public static void main(String[] args) {
            /*
        Write a Java program that asks user to enter a number
        if number is dividable by 5 then finish the program but if number is not dividable by 5,
        keep asking user to enter a new number until user enters a number that is dividable by 5
        Ask user to enter a number while their number is not divided by 5

        Do _______________________while

        Example program:
        Program: Please enter a number
        User: 3
        Program: This number is not dividable by 5

        Program: Please enter a  number
        User: 10
        Program: This number is dividable by 5
         */




        }
    }
}
