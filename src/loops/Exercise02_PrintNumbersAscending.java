package loops;

public class Exercise02_PrintNumbersAscending {
    public static void main(String[] args) {

        /*
        Write a program that prints numbers from 1 to 10 as below
        expected output:
        1-2-3-4-5-6-7-8-9-10
         */

        System.out.println("\n__________________beginner way________________\n");

        for (int i = 1; i <=10; i++){
            if (i >= 1 && i <=9) System.out.print(i + " - ");
            else System.out.println(i);
        }

        System.out.println("\n__________________better way________________\n");

        String result = "";

        for (int i1 = 1; i1 <= 10; i1++) {
            result += i1 + " - ";
        }

        System.out.println(result.substring(0, result.length() -3));



    }
}
