package loops.fori_loops;

public class UnderstandingForILoop {
    public static void main(String[] args) {

        /*
        for(initialization; termination condition; change){
        //code block to be executed
         */


        for (int i = 0; i <=4; i++){ // 0, 1, 2, 3, 4 --> it becomes 5 iterations
            System.out.println("Hello World!");
        }

        System.out.println("End of the program");

        int sum = 0;

        for (int i = 10; i <= 15; i++) {
            sum += i;
        }

    }
}
