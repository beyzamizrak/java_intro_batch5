package loops.fori_loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {

        int i = ScannerHelper.getANumber(), j = ScannerHelper.getANumber();

        System.out.println("\nThe even numbers between " + i + " and " + j + " are as follows: \n");

        for (int k = Math.min(i, j); k <= Math.max(i, j); k++) {
            if(k % 2 == 0) System.out.println(k);

        }

    }
}
