package loops.fori_loops;

import utilities.ScannerHelper;

public class Exercise05_PrintOddNumbersUsingScanner {
    public static void main(String[] args) {

        int i = ScannerHelper.getANumber();

        System.out.println("\n ---The odd numbers from 0 to " + i +  " are as below: \n");

        for (int j = 0; j <= i; j++) {
            if(j % 2 == 1) System.out.println(j);
        }

    }
}
