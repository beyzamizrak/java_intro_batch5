package loops.fori_loops;

public class Exercise02_PrintNumbersDescending {
    public static void main(String[] args) {

        for (int i1 = 100; i1 >= 0; i1--){
            System.out.println(i1);
        }
        System.out.println("End of the program!");

    }
}
