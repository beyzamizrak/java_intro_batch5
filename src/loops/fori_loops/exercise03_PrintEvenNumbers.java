package loops.fori_loops;

public class exercise03_PrintEvenNumbers {
    public static void main(String[] args) {

        // Not recommended
        for (int i2 = 0; i2 <= 10; i2+=2){
            System.out.println(i2);
        }

        System.out.println("End of the program!");

        //recommended way

        for (int i1 = 0; i1 <= 10; i1++) {
            if(i1 % 2 == 0) System.out.println(i1);
        }
        


    }
}
