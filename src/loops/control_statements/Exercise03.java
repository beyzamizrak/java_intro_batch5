package loops.control_statements;

public class Exercise03 {
    public static void main(String[] args) {

        /*
        Print all the even numbers from 1 to 50 included

        expected output: 1
        2
        3
        4
        .
        .
        .
        .
        50
         */

        for (int i = 1; i <= 50; i++) {
            if (i % 2 == 1) continue;
            System.out.println(i);
        }



    }
}
