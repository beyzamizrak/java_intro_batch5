package regex;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Exercises {
    public static void main(String[] args) {

        System.out.println(Pattern.matches("[a-zA-Z0-9_-]{8,15}", "JohnDoe_123"));

        System.out.println(Pattern.matches("[\\d]{3}-[\\d]{2}-[\\d]{4}", "000-00-0000"));

        System.out.println(Pattern.matches("\\([\\d]{3}\\)-[\\d]{3}-[\\d]{4}", "(832)-908-6324"));


        String str = "How much wood would a wood chuck chuck if a wood chuck could chuck a wood";
        System.out.println(str.replaceAll("wood", "****"));

        System.out.println("========task 04================");

        String str1 = " abc 123 $#^ ";

        String str2 = str1.replaceAll("[\\W_]", "");

        System.out.println(str2);

        System.out.println(Pattern.matches("[a-zA-Z]{2,} [a-zA-Z]{2,}", "John Doe"));


        System.out.println("\n=============interview question============\n");

        String myStr = "A1b2C3";

        System.out.println(Arrays.toString(myStr.replaceAll("[^A-Za-z]", "").toCharArray()));
        System.out.println(Arrays.toString(myStr.replaceAll("[^0-9]", "").toCharArray()));



    }
}
