package regex;

import utilities.ScannerHelper;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.regex.Pattern;

public class PatternClass {
    public static void main(String[] args) {

       Pattern pattern = Pattern.compile("[a-z0-9]{5,10}");

        System.out.println(pattern); // gives u the pattern as an object in the compiler
        System.out.println(pattern.pattern()); // returns the pattern as a string
        System.out.println(pattern.toString()); // returns the pattern as a string

        System.out.println(Pattern.matches(pattern.pattern(),"Apple"));


        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a username");
        String str = input.nextLine();

        /*
        // first way
        pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");

        if (Pattern.matches(pattern.toString(), str)) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");
        */

        // second way
        if(str.matches("[a-zA-Z0-9]{5,10}")) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");











    }
}
