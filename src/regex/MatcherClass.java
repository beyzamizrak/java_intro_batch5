package regex;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherClass {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");
        Matcher matcher = pattern.matcher("hello");

        System.out.println(matcher.matches());




        System.out.println("\n-----------new task------------\n");

        String sentence = ScannerHelper.getASentence();
        pattern = Pattern.compile("[A-Za-z]{1,}");
        matcher = pattern.matcher(sentence);


        int counter = 0;

        while(matcher.find()){
            System.out.println(matcher.group()); counter++;
        }
        System.out.println("This sentence contains " + counter + " words");






    }


}
