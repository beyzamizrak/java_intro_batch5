package primitives;

public class Chars {
    public static void main(String[] args) {
        /* primitive char is used to store a single character
        a-z
        A-Z
        0-9
        specials

        Char takes 2 bytes from the memory
        Char should be followed by single quotation
         */

        char space = ' ';
        char dollarSign = '$';
        char percentageSign = '%';
        char upperCaseA = 'A';
        char favChar = '*';
        char firstCharInMyName = 'B';

        System.out.println(upperCaseA);
        System.out.println(favChar);
        System.out.println(dollarSign);


    }
}
