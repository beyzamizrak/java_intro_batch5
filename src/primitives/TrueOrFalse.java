package primitives;

public class TrueOrFalse {
    public static void main(String[] args) {

        /*
        boolean is used to store either true or false
        it will take only one bit

         */

        boolean b1 = true;
        boolean b2 = false;

        System.out.println(b1);
        System.out.println(b2);

    }
}
