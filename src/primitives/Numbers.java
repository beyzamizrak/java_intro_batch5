package primitives;

public class Numbers {
    public static void main(String[] args) {
        System.out.println();

        /*
        There are 6 primitives used to store numbers as data
        byte, short, int, long -> used to store absolute numbers
        float, double -> used for floating numbers like 10.5


        byte -> 1 byte
        short -> 2 bytes
        int -> 4 bytes
        long -> 8 bytes

        NOTE:  Most of the case we use int and its more than enough.
        John
        his age -> 45
        his fav number = 150
        his balance -> 400.45

         */

        // dataType variableName = value;

        System.out.println("\n----Numbers -- byte - short - int - long");
        byte age = 45;
        short favNumber = 150;

        System.out.println(age); //45
        System.out.println(favNumber); //70

        /*
        Floating numbers
        float -> 4 bytes
        double -> 8 bytes
         */

        System.out.println("\n---- Floating Numbers -float- double ---");
        float balance1 = 23.38749384f;
        double balance2 = 23.38749384;
        double price = 23.5;

        System.out.println(price);  // 23.5
    }
}
