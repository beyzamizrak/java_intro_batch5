package string_methods;

public class _09_trim_Method {
    public static void main(String[] args) {

        /*
        1. return
        2. returns string
        3. non-static
        4. it doesn't take any arguments, and it's not overloaded

        This method trims the before spaces and after spaces of characters. it doesn't touch the spaces in between.
         */

        String s1 = "  Hello   ";

        System.out.println(s1.length());
        System.out.println(s1.trim().length());

    }
}
