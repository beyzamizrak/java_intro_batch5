package string_methods;

public class _11_startsWith_endsWith_Methods {
    public static void main(String[] args) {
        /*
        1.return type
        2.returns boolean
        3.non-static
        4.there is overloaded methods but we will always use it with single argument, the one with arg
         */
        String s = "TechGlobal";

        System.out.println(s.startsWith("T"));
        System.out.println(s.startsWith("t"));
        System.out.println(s.startsWith("Tech"));
        System.out.println(s.endsWith("Global"));

        //IMPORTANT
        String city = "Chicago";

        System.out.println(city.startsWith(city)); // true
        System.out.println(city.endsWith(city)); // true
        System.out.println(city.startsWith("")); // true
        System.out.println(city.endsWith("")); // true

        System.out.println(city.startsWith("M")); // false
        System.out.println(city.endsWith("lin")); // false



    }
}
