package string_methods;

public class _13_replace_Method {
    public static void main(String[] args) {
        /*
        return type
        returns string
        non-static
        there are overloaded methods, one takes two chars to be replaced, and the other one takes two Strings to be replaced.
         */

        String s = "Can I can a can";
        String s1 = s.replace('c', 'x');

        System.out.println(s1);

        System.out.println(s.replace("can", "xxx"));
        System.out.println(s.toLowerCase().replace("can", "xxx"));

        // IMPORTANT

        String str1 = "John";
        str1 = str1.replace("o", "oooo");
        System.out.println(str1); // Joooohn

        String str2 = "apple";
        str2 = str2.replace("abc", "xyz");

        System.out.println(str2); // apple

        String str3 = "orange";
        str3 = str3.replace(str3, ""); // ""


    }
}
