package string_methods;

public class _07_indexOf_lastIndexOf_Methods {
    public static void main(String[] args) {
        /*
        1. return
        2. returns int which is matching index
        3. nin-static
        4. it takes a char or string, there are overloaded methods
         */

        String company = "TechGlobal School";

        int firstIndexOfO = company.indexOf('o'); // 6
        int lastIndexOfO = company.lastIndexOf('o'); //15

        System.out.println(firstIndexOfO);
        System.out.println(lastIndexOfO);

        // find the first and last indexes of l

        System.out.println(company.indexOf('l'));
        System.out.println(company.lastIndexOf('l'));

        System.out.println(company.indexOf('X'));

        System.out.println(company.indexOf("School"));






    }
}
