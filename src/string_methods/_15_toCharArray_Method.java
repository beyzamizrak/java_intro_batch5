package string_methods;

import java.util.Arrays;

public class _15_toCharArray_Method {
    public static void main(String[] args) {

        /*
        return type
        always returns a char
        non-static
        doesn't take any arguments
         */
        String s = "TechGlobal School";

        System.out.println(Arrays.toString(s.toCharArray())); // it converts string to a char array and then prints as string

    }
}
