package string_methods;

public class _14_isEmpty_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a boolean
        non-static
        it doesn't take any arguments
         */

         String s1 = ""; // length is equal to 0

        System.out.println(s1.isEmpty()); // true

        String s2 = " ";

        System.out.println(s2.isEmpty()); // false

        String s3 = "Hello";

        System.out.println(s3.isEmpty()); // false
    }
}
