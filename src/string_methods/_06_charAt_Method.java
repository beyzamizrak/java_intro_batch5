package string_methods;

public class _06_charAt_Method {
    public static void main(String[] args) {

        /*
        1. return type
        2. returns char at given index
        3. non-static
        4. it takes an int as argument

         */

        String sentence = "Java is fun";

        char c1 = sentence.charAt(3);
        char c2 = sentence.charAt(4);
        System.out.println(c1); // 'a'
        System.out.println(c2); // ''  -> space

        System.out.println(sentence.charAt(9)); // 'u'

        /* NOTE: Runtime error

        System.out.println(sentence.charAt(-5));
        System.out.println(sentence.charAt(50));
        */

        /*NOTE:
        System.out.println(sentence.charAt(1).toUpperCase()); // not possible, compiler error because
        toUpperCase() method cannot be invoked with a char data-variable
         */
        System.out.println("end of the program");






    }
}
