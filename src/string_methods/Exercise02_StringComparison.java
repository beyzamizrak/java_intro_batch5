package string_methods;

public class Exercise02_StringComparison {
    public static void main(String[] args) {
        /*
        In this task we will be learning different ways of ignoring the different case
        assume that you are given below Strings

        JAVA
        java

         Compare these 2 strings and always get true
         */

        String str1 = "JAVA";
        String str2 = "java";

        System.out.println(str1.equalsIgnoreCase(str2));
        System.out.println(str1.toLowerCase().equals(str2));
        System.out.println(str1.equals(str2.toUpperCase()));




    }
}
