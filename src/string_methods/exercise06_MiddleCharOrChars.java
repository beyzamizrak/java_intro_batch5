package string_methods;

import utilities.ScannerHelper;

public class exercise06_MiddleCharOrChars {
    public static void main(String[] args) {

        /*
        write a program that ask user to enter a name then find middle name if the name has
         */

        String name = ScannerHelper.getAName();


        if (name.length() % 2 == 0){
            System.out.println("Middle chars are = " + name.substring(name.length() / 2 -1 , name.length() / 2 + 1));
        }
        else System.out.println("Middle char is = " + name.charAt(name.length() / 2));





    }
}
