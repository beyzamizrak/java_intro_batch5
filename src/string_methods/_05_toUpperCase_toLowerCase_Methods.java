package string_methods;

public class _05_toUpperCase_toLowerCase_Methods {
    public static void main(String[] args) {
        /*
        for both toLowerCase() method  and toUpperCase() method
        1 return type
        2. returns string
        3. non-static
        4. we choose the one that doesn't take any arguments, and there are overloading methods
         */

        String name = "John";

        System.out.println(name.toLowerCase()); //john
        System.out.println(name.toUpperCase()); //JOHN
        System.out.println("hello".toUpperCase()); // HELLO
        System.out.println("heLLO".toLowerCase()); // hello

        System.out.println("abc".toUpperCase().toLowerCase()); //abc
        System.out.println("ab".toUpperCase().concat("xY".toLowerCase()).toLowerCase()); // abxy





    }
}
