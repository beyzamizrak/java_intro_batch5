package date_time;

import java.sql.SQLOutput;
import java.time.*;
import java.time.temporal.ChronoUnit;

public class DurationAnddPeriod {

    public static void main(String[] args) {


        // find how many days passed since you were born
        // Current date - dateOfBirth
        // How many years passed
        // how many months passed
        // how many weeks passed
        // how many days passed

        LocalDate today = LocalDate.now();
        LocalDate dateOfBirth = LocalDate.of(1989, Month.APRIL, 22);

        System.out.println(today);
        System.out.println(dateOfBirth);

        // This doesn't give exact answers

        System.out.println(Period.between(dateOfBirth, today).getYears()); //33
        System.out.println(Period.between(dateOfBirth, today).getMonths()); // 3
        System.out.println(Period.between(dateOfBirth, today).getDays()); // 19

        // Using CronoUnits

        System.out.println("\n-----------Using ChronoUnit class\n");

        System.out.println(ChronoUnit.YEARS.between(dateOfBirth, today));
        System.out.println(ChronoUnit.MONTHS.between(dateOfBirth, today));
        System.out.println(ChronoUnit.WEEKS.between(dateOfBirth, today));
        System.out.println(ChronoUnit.DAYS.between(dateOfBirth, today));


        LocalTime localtime1 = LocalTime.of(12, 44, 15);
        LocalTime localtime2 = LocalTime.of(13, 45, 45);

        // doesn't give exact answer

        System.out.println(Duration.between(localtime1, localtime2).getSeconds());

        //ChronoUnit gives exact answers

        System.out.println("\n-----------Using ChronoUnit class\n");


        System.out.println(ChronoUnit.HOURS.between(localtime1, localtime2));
        System.out.println(ChronoUnit.MINUTES.between(localtime1, localtime2));
        System.out.println(ChronoUnit.SECONDS.between(localtime1, localtime2));





    }
}
