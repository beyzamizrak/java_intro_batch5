package print_statements;

public class PrintVsPrintln {
    public static void main(String[] args){
        // println
        // println prints the given text and moves cursor to the next line

        System.out.println("This is println");
        System.out.println("Beyza Mizrak");
        System.out.println("Today is Sunday");

        //print
        System.out.print("This is println");
        System.out.print("Beyza Mizrak");
        System.out.print("Today is Sunday");



    }
}
