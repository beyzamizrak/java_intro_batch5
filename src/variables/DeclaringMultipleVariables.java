package variables;

public class DeclaringMultipleVariables {
    public static void main(String[] args) {

        // declaring multiple variables at the same time
        int age1, age2, age3;

        // declaring multiple variables and assigning at the same time
        double d1 = 25, d2 =10, d3 = 2.5;

        //declaring multiple variables and assigning some of them
        boolean b1 = true, b2, b3 = false;







    }

}
