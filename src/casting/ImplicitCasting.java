package casting;

public class ImplicitCasting {

    public static void main(String[] args) {


        // implicit casting = widening = upcasting

        byte b = 25; // -128, 127
        int i = b; // implicit casting

        System.out.println(i); // 25

        float f = 10.5f;

        double d = f;

        System.out.println(d); //10.5















    }
}
