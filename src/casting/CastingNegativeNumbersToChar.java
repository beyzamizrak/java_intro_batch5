package casting;

public class CastingNegativeNumbersToChar {

    public static void main(String[] args) {

        int negative = -32;

        char myChar = (char) negative;
        System.out.println("Negative number char value = " + myChar);

        char zero = 48;
        System.out.println("Zero = " + zero);

        char upperA = 65;
        char upperZ = 90;

        System.out.println("Uppercase A = " + upperA);
        System.out.println("Uppercase Z = " + upperZ);

        char myCharO = 'O';
        int oAsciiValue = (int) myCharO;

        System.out.println("Uppercase O = " + myCharO);
        System.out.println("O ASCII value = " + oAsciiValue);

        if (oAsciiValue >= 65 && oAsciiValue <= 90) System.out.println("Your char is uppercase");
        else {
            System.out.println("Your char is not uppercase");

        }


    }
}