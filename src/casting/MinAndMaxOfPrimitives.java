package casting;

public class MinAndMaxOfPrimitives {
    public static void main(String[] args) {


        System.out.println("Min value of byte = " +  Byte.MIN_VALUE);
        System.out.println("Max value of byte = " + Byte.MAX_VALUE);

        System.out.println("Min value of short = " +  Short.MIN_VALUE);
        System.out.println("Max value of short = " + Short.MAX_VALUE);

        System.out.println("Min valur of integer = " + Integer.MIN_VALUE);
        System.out.println("Max valur of integer = " + Integer.MAX_VALUE);

    }
}
