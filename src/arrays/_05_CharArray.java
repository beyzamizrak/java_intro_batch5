package arrays;

import java.util.Arrays;

public class _05_CharArray {
    public static void main(String[] args) {

        /*
        TASK-1
        Create a char array and store values below
        #
        $
        5
        A
        b
        H
        Print the array
        EXPECTED:
        [#, $, 5, A, b, H]
        */

        System.out.println("\n--------------------TASK-1---------------------\n");

        char[] characters = {'#', '$', '5', 'A', 'b', 'H'};

        System.out.print(Arrays.toString(characters));

        System.out.println("\n--------------------TASK-2---------------------\n");

        System.out.println("The size of the array = " + characters.length);

        System.out.println("\n--------------------TASK-3---------------------\n");


        for (int i = 0; i < characters.length; i++) {
            System.out.println(characters[i]);
        }

        System.out.println("\n--------------------TASK-4---------------------\n");

        for (char character : characters) {
            System.out.println(character);

        }

        System.out.println("\n--------------------TASK-4---------------------\n");

        for (char character : characters) {
            System.out.println(character);

        }

        System.out.println("\n--------------------TASK-5---------------------\n");

        for (int i = 0; i < characters.length; i++) {
            if (Character.isLetter(characters[i])) System.out.println(characters);
        }

        for (char character : characters) {
            if (Character.isLetter(character)) System.out.println(character);
        }

        System.out.println("\n--------------------TASK-6---------------------\n");

        for (char character : characters) {
            if(Character.isLetter(character)){
                if(Character.isUpperCase(character)) System.out.println(character);
            }

        }

        int countU1 = 0;
        for (char character : characters) {
            if(Character.isUpperCase(character)) countU1++;
        }
        System.out.println(countU1);

        int countU2 = 0;

        for (int i = 0; i < characters.length; i++) {
            if(Character.isUpperCase(characters[i])) countU2++;
        }
        System.out.println(countU2);


        }



    }

