package arrays;

import java.util.Arrays;

public class TwoDimensionalArray {
    public static void main(String[] args) {

        String[][]  students = {
                {"Ali", "Mehmet", "Alex"},
                {"Alex", "Regina"},
                {"Abdallah", "Newer"}
        };

        System.out.println(students[0][1]); // Mehmet
        System.out.println(students[1][1]); // Regina
        System.out.println(students.length); //3
        System.out.println(Arrays.toString(students[0])); // [Ali, Mehmet, Alex]
        System.out.println(Arrays.toString(students[1])); // [Ali, Regina]
        System.out.println(Arrays.toString(students[2])); // [Abdallah, Newer]

        for (int i = 0; i < students.length; i++) {
            System.out.println(Arrays.toString(students[i]));
        }

        for (String[] student : students) {
            System.out.println(Arrays.toString(student));

        }

        // printing the multidimensional array
        System.out.println(Arrays.deepToString(students));


        /*
        print each member in different line
         */


        for (String[] group : students) {
            System.out.println(Arrays.toString(group));
            for (String student : group) {
                System.out.println(student);

            }
        }


    }
}
