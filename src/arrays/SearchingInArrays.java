package arrays;

import java.util.Arrays;

public class SearchingInArrays {
    public static void main(String[] args) {

        int[] numbers = {3, 5, 8, 10};

        // search for 7 and return true if it exists in this array return false otherwise

        boolean has7 = false;

        for (int number : numbers) {
            if (number == 7) {
                has7 = true;
                break;
            }
            System.out.println(has7); // false
        }
        // search for 5 and return true if it exist in this array

        boolean has5 = false;

        for (int number : numbers) {
            if (number == 5) {
                has5 = true;
                break;
            }
            System.out.println(has5); // true
        }


        System.out.println("\n=====binary search in arrays======\n");

        //NOTE: Binary search can be used if the array is sorted ONLY, otherwise you will get wrong results.
        // Binary search returns the index of the element if found
        // 3, 5, 8, 10
        // if elements exist twice than the method returns the last index imagine array has 3, 5, 8, 10, 10

        Arrays.sort(numbers);
        System.out.println(Arrays.binarySearch(numbers, 5));
        System.out.println(Arrays.binarySearch(numbers, 10)); // 3
        System.out.println(Arrays.binarySearch(numbers, 1)); // -something

    }
}
