package arrays;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class MyPractice {

    public static String[] removeDuplicates(String[] arr){

        LinkedHashSet<String> newList = new LinkedHashSet<>(Arrays.asList(arr));

       return new LinkedHashSet<>(Arrays.asList(arr)).toArray(new String[0]);
    }

    public static void main(String[] args) {

        String[] arr = {"Java", "JavaScript", "Ruby", "Go", "Java", "Ruby", "Java"};

        System.out.println((Arrays.toString(removeDuplicates(arr))));
    }
}
