package arrays;

import java.util.Arrays;

public class Exercise01_CountChar {
    public static void main(String[] args) {

        /*
        String word = "Java";
        count how many 'a' letters in the string
        */

        String word = "Java";

        int counter = 0;

        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == 'a') counter++;

        }
        System.out.println(counter);

        System.out.println("End of the program");


        char[] wordArray = word.toCharArray();

        System.out.println(Arrays.toString(wordArray));

        int counter1 = 0;

        for (char element : wordArray) {
            if (element == 'a') counter1++;
        }
        System.out.println(counter1);




    }
}
