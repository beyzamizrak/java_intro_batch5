package arrays;

public class _04_CountElements_InIntArrays {
    public static void main(String[] args) {


        int[] numbers = { -1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        /*
        negatives:2
        positives:5
        neutral or zeros: 3
        even:6
        odd:4
        max:10
        min:-7
        sum of the numbers: 28
        avarage of the numbers: 2
        how many unique numbers u have: 7
        how many of these numbers are required in fibonachi sequences: 6
        prime numbers: 2
        how many numbers can be divided by 5: 6
        absolute difference between the max and min of these numbers: 17
        closest numbers to 9(take the left if u have two): 8
        find the max in the first 5 elements: 5
        create a new array that only has positive numbers
        create a new array that only has negative numbers
         */

        int negatives = 0;

        for(int number: numbers){
            if (number < 0)
                negatives++;
        }
        System.out.println("Negative count is = " + negatives);

        int positives = 0;

        for(int number: numbers){
            if (number > 0)
                positives++;
        }
        System.out.println("Positive count is = " + positives);

        int neutralOrZeros = 0;

        for (int number : numbers) {
            if (number == 0)
                neutralOrZeros++;

        }
        System.out.println("Neutral count is = " + neutralOrZeros);

        // count how many even numbers you have in above array

        int counter1 = 0;

        for (int number : numbers) {
            if(number % 2 == 0) counter1++;

        }

        System.out.println(counter1);

    }
}
