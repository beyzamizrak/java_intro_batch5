package arrays;

import java.util.Arrays;

public class _01_StringArray {
    public static void main(String[] args) {

        // 1. declare a String array called as countries and assign size of 3

        String[] countries = new String[3];

        // 2. assigning values to specific indexes

        countries[1] = "Spain";

        // print a specific index from an array

        System.out.println(countries[0]); // null
        System.out.println(countries[1]); // Spain
        System.out.println(countries[2]); // null

        // printing an array

        System.out.println(Arrays.toString(countries)); // [null, Spain, null]

        // assign Belgium to index of 2 and print the array

        countries[2] = "Belgium";

        System.out.println(Arrays.toString(countries)); // [null, Spain, Belgium]

        // update existing element

        countries[1] = "France";

        System.out.println(Arrays.toString(countries)); // [null, France, Belgium]

        //getting the length of an array -  how many elements

        System.out.println(countries.length); // 3

        // printing each element separately

        System.out.println(countries[0]);
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        for (int i = 0; i < countries.length; i++) {
            System.out.println("Country at index of " + i + " is = " + countries[i]);
        }



    }
}
