package arrays;

import java.util.Arrays;

public class _06_doubleArray_Recap {
    public static void main(String[] args) {

        // create double array using curly braces

        double[] decimals = {1.5, 2.3, -1.3, -3.7};
        char[] characters = {'a', 'b', 'c', 'd'};


        System.out.println(Arrays.toString(decimals));

        System.out.println("The length of the array is "  + decimals.length);

        Arrays.sort(decimals);

        System.out.println("Arrays after sorting --> " + Arrays.toString(decimals));

        System.out.println("\n-------------fori loop way==========");
        for (int i = 0; i < decimals.length; i++) {
            System.out.println(decimals[i]);
        }
        System.out.println("\n-------------foreach loop way==========");
        for (double decimal : decimals) {
            System.out.println(decimal);
        }

        System.out.println("\n-------------for i loop print two arrays==========");
        for (int i = 0; i < decimals.length; i++) {
            System.out.println(decimals[i] + " - " + characters[i]);
        }






    }
}
