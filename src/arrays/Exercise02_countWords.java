package arrays;

public class Exercise02_countWords {
    public static void main(String[] args) {
        /*
        String sentence = "I love arrays";
         */

        String sentence = "I love arrays";

        System.out.println("\n==================for i loop way===================\n");

        int counter = 0;

        for (int i = 0; i < sentence.length(); i++) {
            if(sentence.charAt(i) == ' ') counter++;

        }
        System.out.println(counter + 1);

        System.out.println("\n==================for each loop way with toCharArray===================\n");

        int counter2 = 0;

        for (char element : sentence.toCharArray()) {
            if (element == ' ') counter2++;

        }
        System.out.println(counter2 + 1);

        System.out.println("\n==================split() method way===================\n");




    }
}
