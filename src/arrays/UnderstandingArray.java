package arrays;

public class UnderstandingArray {
    public static void main(String[] args) {
        /*
        Declare a String and store a name in it

        Declare different Strings and store the names of your best  friends in them

        Declare different Strings and store the names of all your friends in them

         */

        String name = "James";

        String[] names = {"Beyza", "Andrii", "Vlad", "Samir", "Olena"};

        // Retrieving an element from an array - using index
        System.out.println(names[2]); // Vlad
        System.out.println(names[4]); // Olena

        // ArrayIndexOutOfBoundException

        //System.out.println(names[5]); // ArrayIndexOutOfBoundException
        //System.out.println(names[-3]); // ArrayIndexOutOfBoundException

        int age = 25;
        int[] ages = {21, 23, 25};

        System.out.println(ages[2]); // 25











    }
}
