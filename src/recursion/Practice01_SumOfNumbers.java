package recursion;

public class Practice01_SumOfNumbers {

    /*
    create a public static method called sum
    it takes and int which is a positive number
    it returns the sum of numbers starting from 1 to given unt

    5 -> 1+ 2 + 3 + 4 + 5 -> 15

     */

    //iterative way

    public static int sum(int num){

        int sum = 0;
        for (int i = 1; i <= num ; i++) {
            sum+= i;
        }
        return sum;
    }

    // recursive way

    public static int sumRecursively(int end){
        if(end != 0) return end + sumRecursively(end - 1); // 5 + 4 + 3 + 2 + 1 + 0
        return 0;
    }



    /*
    create a public static method called product
    it takes and int which is a positive number
    it returns the product of numbers starting from 1 to given unt

    5 -> 5 * 4 * 3 * 2 * 1
     */

    public static int productRecursively(int end){
        if(end != 1) return end * productRecursively(end -1);
        return 0;
    }


    public static void main(String[] args) {

        System.out.println(sum(5));
        System.out.println(sum(6));
        System.out.println(sum(8));

        System.out.println(sumRecursively(5));
        System.out.println(sumRecursively(6));
        System.out.println(sumRecursively(8));

        System.out.println(productRecursively(5));
        System.out.println(productRecursively(4));
    }

}
