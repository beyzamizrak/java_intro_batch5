package recursion;

import enum_practices.Days;

public class Practice02_ReverseString {

    /*
    create a methid called reverse
    it takes a string and returnd it back reversed
    use recursion

     */

    public static String reverse(String str){
        if(str.length() < 2) return str;
        return reverse(str.substring(1)) + str.charAt(0) ;
    }

    public static void main(String[] args) {
        System.out.println(reverse("Hello"));
    }


}
