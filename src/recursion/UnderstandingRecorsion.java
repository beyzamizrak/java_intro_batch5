package recursion;

public class UnderstandingRecorsion {

    public static void print(){

        System.out.println("Hello");
        print();
    }


    // create a method that prints number from 1 to given number

    public static void printNumbers(int number){

        for (int i = 1; i <= number; i++) {
            System.out.println(i);;
        }
    }

    // recursive way

    public static void printNumbersRecursively(int end){
        if(end == 0 ) return;
        printNumbersRecursively(end - 1);
        System.out.println(end);
    }




    public static void main(String[] args) {
        //print(); // stackOverFlowError

        printNumbers(5);
        printNumbersRecursively(5);


    }



}
