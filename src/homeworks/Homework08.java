package homeworks;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {

        System.out.println("\n================TASK-1=================\n");
        System.out.println(countConsonants("HelloWorld"));
        System.out.println("\n================TASK-2=================\n");
        String str = "hello is fun";
        System.out.println(Arrays.toString(wordArray(str)));
        System.out.println("\n================TASK-3=================\n");
        String newStr = "Hello,    nice to   meet     you!!";
        System.out.println(removeExtraSpaces(newStr));
        System.out.println("\n================TASK-4=================\n");
        System.out.println(count3OrLess());
        System.out.println("\n================TASK-5=================\n");
        String dateOfBirth = "1/20/1991";
        System.out.println(isDateFormatValid(dateOfBirth));
        System.out.println("\n================TASK-6=================\n");
        String email = "abcd@@gmail.com";
        System.out.println(isEmailFormatValid(email));




    }


    public static int countConsonants(String str){

        String newStr = str.replaceAll("[aeiouAEIOU]+", "");

         return newStr.length();

    }

    public static String[] wordArray(String str){
        String newStr = str.trim();
        String[] arr = {newStr};

        if (newStr.contains(" ")){
            return newStr.split(" ");
        }
        else return arr;
    }

    public static String removeExtraSpaces(String str){

       String newStr = str.replaceAll("[\\s]+", " ").trim();
       return newStr;

    }

    public static int count3OrLess(){

        String sentence = ScannerHelper.getASentence();
        String[] sentenceArr = sentence.split( " ");

        int counter = 0;

        for (String s : sentenceArr) {
            if(s.matches("[a-zA-Z]{1,3}")) counter++;
        }
        return counter;

    }

    public static boolean isDateFormatValid(String dateOfBirth){

        if (dateOfBirth.matches("[\\d]{2}/[\\d]{2}/[\\d]{4}")) return true;
        else return false;

    }

    public static boolean isEmailFormatValid(String email){

        return email.matches("[a-z]{2,}@[a-z]{2,}.*[a-z]{2,}");
    }













}
