package homeworks;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {

    // TASK 1
    public static int countWords(String str){

        String str1 = str.trim();
        String[] words = str1.split(" ");

        if (str.length() == 0) return 0;
        else return words.length;
    }

    // TASK 2
    public static int countA(String str){

        int countA = 0;

        for (int i = 0; i < str.length(); i++) {

            if(str.toLowerCase().charAt(i) == 'a') countA++;
        }

        if(str.isEmpty()) return 0;
        return countA;
    }

    //TASK 3
    public static int countPos(ArrayList<Integer> numbers){

        int counter = 0;

        for (Integer number : numbers) {
            if(number > 0) counter++;
        }
        return counter;
    }

    // TASK 4
    public static ArrayList<Integer> removeDuplicateNumbers (ArrayList<Integer> numbers){
        ArrayList<Integer> newArr = new ArrayList<>();

        for (Integer number : numbers) {
            if(!newArr.contains(number)) newArr.add(number);
        }
        return newArr;
    }

    // TASK 5
    public static ArrayList<String> removeDuplicateElements (ArrayList<String> elements){
        ArrayList<String> newArr = new ArrayList<>();

        for (String element : elements) {
            if(!newArr.contains(element)) newArr.add(element);
        }
        return newArr;
    }

    // TASK 6
    public static String removeExtraSpaces(String str){
        return str.trim().replaceAll(" +", " ");
    }

    // TASK 7
    public static int[] add(int[] numbers1, int[] numbers2){
        int[] numbers3 = new int[(int)Math.max(numbers1.length, numbers2.length)];

        for (int i = 0; i < numbers3.length; i++) {
            if(numbers1.length > i) numbers3[i] += numbers1[i];
            if(numbers2.length > i) numbers3[i] += numbers2[i];
        }
        return numbers3;

    }
    // TASK 8
    public static int findClosestTo10(int[] numbers){

        if (numbers.length < 0) System.out.println("There is not enough numbers in the array");

        int closest = 10;

        for (int i = 0; i < numbers.length; i++) {
            if(closest == 10) closest = numbers[i];
            else if ( numbers[i] > 0 && numbers[i] < Math.abs(closest)) closest = numbers[i];
        }
        return closest;
    }


    public static void main(String[] args) {
        System.out.println("\n_________________task 1-----------------\n");

        String str = "   java is fun ";

        System.out.println(countWords(str));

        System.out.println("\n_________________task 2-----------------\n");

        String str1 = "TechGlobal is a QA bootcamp";

        System.out.println(countA(str1));

        System.out.println("\n_________________task 3-----------------\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67));

        System.out.println(countPos(numbers));

        System.out.println("\n_________________task 4-----------------\n");

        ArrayList<Integer> numbers1 = new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60));

        System.out.println(removeDuplicateNumbers(numbers1));

        System.out.println("\n_________________task 5-----------------\n");

        ArrayList<String> words = new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"));

        System.out.println(removeDuplicateElements(words));

        System.out.println("\n_________________task 6-----------------\n");

        String str2 = "     I    am     learning   Java   ";

        System.out.println(removeExtraSpaces(str2));

        System.out.println("\n_________________task 7-----------------\n");

        int[] arr1 = {3, 0, 0, 7, 5, 10};
        int[] arr2 = {6, 3, 2};

        System.out.println(Arrays.toString(add(arr1, arr2)));

        System.out.println("\n_________________task 8-----------------\n");

        int[] numbers2 = {10, -13, 5, 70, 15, 57};

        System.out.println(findClosestTo10(numbers2));
    }
}
