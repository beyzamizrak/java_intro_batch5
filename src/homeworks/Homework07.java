package homeworks;

import java.util.ArrayList;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n------------------------------------TASK_1------------------------------------\n");

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(10);
        numbers.add(23);
        numbers.add(67);
        numbers.add(23);
        numbers.add(78);

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n------------------------------------TASK_2------------------------------------\n");

        ArrayList<String> colors = new ArrayList<>();

        colors.add("Blue");
        colors.add("Brown");
        colors.add("Red");
        colors.add("White");
        colors.add("Black");
        colors.add("Purple");

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n------------------------------------TASK_3------------------------------------\n");

        ArrayList<Integer> numbers1 = new ArrayList<>();

        numbers1.add(23);
        numbers1.add(-34);
        numbers1.add(-56);
        numbers1.add(0);
        numbers1.add(89);
        numbers1.add(100);


        System.out.println(numbers1);
        Collections.sort(numbers1);
        System.out.println(numbers1);

        System.out.println("\n------------------------------------TASK_4------------------------------------\n");

        ArrayList<String> cities = new ArrayList<>();

        cities.add("Istanbul");
        cities.add("Berlin");
        cities.add("Madrid");
        cities.add("Paris");

        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n------------------------------------TASK_5------------------------------------\n");

        ArrayList<String> marvelChars = new ArrayList<>();

        marvelChars.add("Spider Man");
        marvelChars.add("Iron Man");
        marvelChars.add("Black Panter");
        marvelChars.add("Dead Pool");
        marvelChars.add("Captain America");

        System.out.println(marvelChars);

        System.out.println(cities.contains("Wolwerine"));

        System.out.println("\n------------------------------------TASK_6------------------------------------\n");

        ArrayList<String> avengersChars = new ArrayList<>();

        avengersChars.add("Hulk");
        avengersChars.add("Black Widow");
        avengersChars.add("Captain America");
        avengersChars.add("Iron Man");

        Collections.sort(avengersChars);

        System.out.println(avengersChars);

        System.out.println(avengersChars.contains("Hulk") && avengersChars.contains("Iron Man"));

        System.out.println("\n------------------------------------TASK_7------------------------------------\n");

        ArrayList<Character> characters = new ArrayList<>();

        characters.add('A');
        characters.add('x');
        characters.add('$');
        characters.add('%');
        characters.add('9');
        characters.add('*');
        characters.add('+');
        characters.add('F');
        characters.add('G');

        System.out.println(characters);

        for (Character character : characters) {
            System.out.println(character);
        }
        System.out.println("\n------------------------------------TASK_8------------------------------------\n");

        ArrayList<String> officeSupplies = new ArrayList<>();

        officeSupplies.add("Desk");
        officeSupplies.add("Laptop");
        officeSupplies.add("Mouse");
        officeSupplies.add("Monitor");
        officeSupplies.add("Mouse-Pad");
        officeSupplies.add("Adapter");

        System.out.println(officeSupplies);
        Collections.sort(officeSupplies);
        System.out.println(officeSupplies);

        int countM = 0;
        int countNoAOrE = 0;

        for (String officeSupply : officeSupplies) {
            if(officeSupply.toLowerCase().startsWith("m")) {
                countM++;
            }
            if(!(officeSupply.toLowerCase().contains("a") || officeSupply.toLowerCase().contains("e"))){
                countNoAOrE++;
            }

        }
        System.out.println(countM);
        System.out.println(countNoAOrE);


        System.out.println("\n------------------------------------TASK_9------------------------------------\n");

        ArrayList<String> kitchenObjects = new ArrayList<>();

        kitchenObjects.add("Plate");
        kitchenObjects.add("spoon");
        kitchenObjects.add("fork");
        kitchenObjects.add("Knife");
        kitchenObjects.add("cup");
        kitchenObjects.add("Mixer");

        System.out.println(kitchenObjects);

        int countU = 0;
        int countL = 0;
        int countHasP = 0;
        int countStartsEndsWithB = 0;

        for (String kitchenObject : kitchenObjects) {
            if(Character.isUpperCase(kitchenObject.charAt(0))) countU++;
            if(Character.isLowerCase(kitchenObject.charAt(0))) countL++;
            if(kitchenObject.toLowerCase().contains("p")) countHasP++;
            if(kitchenObject.toLowerCase().startsWith("p") || kitchenObject.toLowerCase().endsWith("p"))
                countStartsEndsWithB++;

        }
        System.out.println("Elements start with uppercase = " + countU);
        System.out.println("Elements start with lowercase = " + countL);
        System.out.println("Elements having P or p = " + countHasP);
        System.out.println("Elements starting or ending with P or p = " + countStartsEndsWithB);

        System.out.println("\n------------------------------------TASK_10------------------------------------\n");

        ArrayList<Integer> numbers2 = new ArrayList<>();

        numbers2.add(3);
        numbers2.add(5);
        numbers2.add(7);
        numbers2.add(10);
        numbers2.add(0);
        numbers2.add(20);
        numbers2.add(17);
        numbers2.add(10);
        numbers2.add(23);
        numbers2.add(56);
        numbers2.add(78);

        System.out.println(numbers2);

        int count10 = 0;
        int countGreaterThan15 = 0;
        int countOddLessThan20 = 0;
        int countlessThan15GreaterThan50 = 0;

        for (Integer n : numbers2) {
            if( n % 10 == 0) count10++;
            if((n % 2 == 0)&& (n > 15)) countGreaterThan15++;
            if((n % 2 == 1) && (n < 20)) countOddLessThan20++;
            if((n < 15) || ( n > 50)) countlessThan15GreaterThan50++;
        }
        System.out.println("Elements that can be divided by 10 = " + count10);
        System.out.println("Elements that are even and greater than 15 = " + countGreaterThan15);
        System.out.println("Elements that are odd and less than 20 = " + countOddLessThan20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + countlessThan15GreaterThan50);

    }
}
