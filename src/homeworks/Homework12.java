package homeworks;

import java.util.*;

public class Homework12 {


    //TASK1

    public static String noDigit(String str){

        String newStr ="";
        for (int i = 0; i < str.length(); i++) {
            if(!(Character.isDigit(str.charAt(i)))) newStr += str.charAt(i);
        }
        return newStr;
    }

    //TASK2
    public static String noVowel(String str){
        String newStr = "";
        for (int i = 0; i < str.length(); i++) {
            if(!(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' ||
                    str.charAt(i) == 'u' || str.charAt(i) == 'A' || str.charAt(i) == 'E' || str.charAt(i) == 'I' ||
                    str.charAt(i) == 'O' || str.charAt(i) == 'U')) newStr+= str.charAt(i);
        }
        return newStr;
    }

    //TASK3

    public static int sumOfDigits(String str){

        int sum = 0;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isDigit(str.charAt(i))) sum+= Character.getNumericValue(str.charAt(i));
        }

        return sum;
    }

    //TASk4

    public static boolean hasUpperCase(String str){

        for (int i = 0; i < str.length(); i++) {
            if(Character.isUpperCase(str.charAt(i))) return true;
        }
        return false;
    }

    //TASK5

    public static int middleInt(int a, int b, int c){
        int[] numbers = {a, b, c};
        Arrays.sort(numbers);
        return numbers[1];
    }

    //TASK6

    public static int[] no13(int[] arr) {

        int[] newArr = new int[arr.length];

        if (arr.length > 0)
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == 13) {
                    newArr[i] = 0;
                } else newArr[i] = arr[i];

            }
        return newArr;
    }

    //TASK7

    public static int[] arrFactorial(int[] numbers){

        int[] numbers2 = new int[numbers.length];
        int f;
        for (int i = 0; i < numbers.length; i++) {
            f = 1;
            for (int j = 1; j <= numbers[i]; j++) {
                f = f * j;
            }
            numbers2[i] = f;
        }
        return numbers2;
    }


    //TASK8
    public static String[] categorizeCharacters(String str){

        String[] newStr = {"", "", ""};

        for (int i = 0; i < str.length(); i++) {
            if(Character.isLetter(str.charAt(i))) newStr[0] += str.charAt(i);
            else if(Character.isDigit(str.charAt(i))) newStr[1] += str.charAt(i);
            else newStr[2] += str.charAt(i);
        }
        return newStr;
    }


    public static void main(String[] args) {

        System.out.println("\n-------------------------TASK1-------------------------\n");

        String str = "123Hello World149";
        System.out.println(noDigit(str));

        System.out.println("\n-------------------------TASK2-------------------------\n");

        String str1 = "TechGlobal";
        System.out.println(noVowel(str1));

        System.out.println("\n-------------------------TASK3-------------------------\n");

        String str2 = "John’s age is 25";

        System.out.println(sumOfDigits(str2));

        System.out.println("\n-------------------------TASK4-------------------------\n");

        String str3 = "Java is Fun";

        System.out.println(hasUpperCase(str3));

        System.out.println("\n-------------------------TASK5-------------------------\n");

        System.out.println(middleInt(1, 23, 10));

        System.out.println("\n-------------------------TASK6-------------------------\n");

        int[] numbers = {13, 2, 3};

        System.out.println(Arrays.toString(no13(numbers)));

        System.out.println("\n-------------------------TASK7-------------------------\n");

        int[] numbers2 = {5, 0, 6 };

        System.out.println(Arrays.toString(arrFactorial(numbers2)));


        System.out.println("\n-------------------------TASK8-------------------------\n");

        String str4 ="12ab$%3c%";

        System.out.println(Arrays.toString(categorizeCharacters(str4)));

    }
    
}
