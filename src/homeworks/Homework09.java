package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09 {
    public static void main(String[] args) {

        System.out.println("\n-----------------------------TASK1-----------------------------\n");

        int[] numbers = {-4, -7, 0, 5, 10, 45};

        boolean isFound = false;
        int firstDuplicate = 0;
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length ; j++) {

                if (numbers[i] == (numbers[j]) && !isFound) {
                    firstDuplicate = numbers[i];
                    isFound = true;
                    break;
                }
            }
        }
        if (isFound) System.out.println(firstDuplicate);
        else System.out.println("There is no duplicate");

        System.out.println("\n-----------------------------TASK2-----------------------------\n");

        String[] words = {"Z", "abc", "z", "123", "#"};

        boolean isFound2 = false;
        String firstDuplicate2 = "";

        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equalsIgnoreCase(words[j]) && !isFound2) {
                    firstDuplicate2 = words[i];
                    isFound2 = true;
                    break;
                }
            }
        }
        if (isFound2) System.out.println(firstDuplicate2);
        else System.out.println("There is no duplicate");

        System.out.println("\n-----------------------------TASK3-----------------------------\n");

        int[] numbers2 = {0, -4, -7, 0, 5, 10, 45, -7, 0};

        String duplicate3 = "";

        for (int i = 0; i < numbers2.length; i++) {
            for (int j = i + 1; j < numbers2.length ; j++) {

                if (numbers2[i] == (numbers2[j]) && !duplicate3.contains(numbers2[i] + "")) {
                    duplicate3 += numbers2[i];
                    System.out.println(numbers2[i]);
                }
            }
        }

        if (duplicate3.isEmpty()) System.out.println("There is no duplicates");

        System.out.println("\n-----------------------------TASK4-----------------------------\n");

        String[] dupWords = {"A", "foo", "12" , "Foo", "bar", "a", "a", "java"};

        String duplicates4 = "";

        for (int i = 0; i < dupWords.length; i++) {
            for (int j = i + 1; j < dupWords.length; j++) {
                if (dupWords[i].equals(dupWords[j].toLowerCase()) && !duplicates4.contains(dupWords[i])) {
                    duplicates4 += dupWords[i] + "\n";
                    System.out.println((dupWords[i]));
                }
            }
        }
        if (duplicates4.isEmpty()) System.out.println("There is no duplicates");

        System.out.println("\n-----------------------------TASK5-----------------------------\n");

        String[] words1 = {"abc", "foo", "bar"};

        ArrayList<String> words2 = new ArrayList<>(Arrays.asList("abc", "foo", "bar"));
        ArrayList<String> words3 = new ArrayList<>();

        for (int i = words2.size()-1; i >= 0 ; i--) {
            words3.add(words2.get(i));

        }
        System.out.println(words3);

        System.out.println("\n-----------------------------TASK6-----------------------------\n");

        String str = "Java is fun";

        String[] sentence = str.split(" ");
        String newSentence = "";

        for (int i = 0; i < sentence.length; i++) {
            String word = sentence[i];
            String reword = "";
            for (int j = word.length() -1; j >= 0; j--) {
                reword += word.charAt(j);
            }
            newSentence += reword + " ";

        }
        System.out.println(newSentence);

    }
}
