package homeworks;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Homework16 {

    public static void main(String[] args) {

        System.out.println("__________________TASK1_______________");
        System.out.println(parseData("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London"));
        System.out.println("__________________TASK2_______________");
        Map<String, Double> price = new TreeMap<>();
        price.put("Apple", 2.00);
        price.put("Orange", 3.29);
        price.put("Mango", 4.99);
        price.put("Pineapple", 5.25);
        Map<String, Integer> quantity = new TreeMap<>();
        quantity.put("Apple",2);
        quantity.put("Pineapple",1);
        quantity.put("Orange",3);
        System.out.println(calculateTotalPrice1(price, quantity));
        System.out.println("__________________TASK3_______________");
        LinkedHashMap<String, Integer> map3 = new LinkedHashMap<>();
        map3.put("Apple", 3);
        map3.put("Mango", 5);
        System.out.println(calculateTotalPrice2(map3));
        LinkedHashMap<String, Integer> map4 = new LinkedHashMap<>();
        map4.put("Apple", 4);
        map4.put("Mango", 8);
        map4.put("Orange", 3);
        System.out.println(calculateTotalPrice2(map4));

    }

    public static Map<String, String> parseData(String str) {
        Map<String, String> map = new TreeMap<>();
        String[] parts = str.split("\\{");
        for (String part : parts) {
            String[] keyValue = part.split("\\}");
            if (keyValue.length == 2) {
                map.put(keyValue[0], keyValue[1]);
            }
        }
        return map;
    }


    public static double calculateTotalPrice1(Map<String, Double> price, Map<String, Integer> quantity){

        double total = 0;
        for(Map.Entry<String, Double> x : price.entrySet()){
            for(Map.Entry<String, Integer> y : quantity.entrySet()){
                if(x.getKey().equals(y.getKey())) total += x.getValue() * y.getValue();
            }
        }return total;
    }

    public static double calculateTotalPrice2(LinkedHashMap<String, Integer> map){
        double total = 0;
        LinkedHashMap<String, Double> priceMap = new LinkedHashMap<>();
        priceMap.put("Apple", 2.00);
        priceMap.put("Mango", 4.99);
        priceMap.put("Orange", 3.29);


        for (Map.Entry<String, Integer> s : map.entrySet()) {
            for (Map.Entry<String, Double> p: priceMap.entrySet()) {
                if(s.getKey().equals(p.getKey())) {
                    if (s.getKey().equals("Mango")) {
                        int freeMango = s.getValue() / 3;
                        total += s.getValue() * p.getValue() - freeMango * p.getValue();
                    } else if (s.getKey().equals("Apple")) {
                        int discountApple = s.getValue() / 2;
                        total += s.getValue() * p.getValue() - discountApple * (0.5 * p.getValue());
                    } else
                        total += s.getValue() * p.getValue();
                }
            }
        }
        return total;
    }

}
