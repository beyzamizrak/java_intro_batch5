package homeworks;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class Homework15 {

    public static void main(String[] args) {

        System.out.println("==============TASK-1===============");
        System.out.println(Arrays.toString(fibonacciSeries1(3))); // [0, 1, 1]
        System.out.println(Arrays.toString(fibonacciSeries1(5))); // [0, 1, 1, 2, 3]
        System.out.println(Arrays.toString(fibonacciSeries1(7))); // [0, 1, 1, 2, 3, 5, 8]
        System.out.println("==============TASK-2===============");
        System.out.println((fibonacciSeries2(2))); //1
        System.out.println((fibonacciSeries2(4))); //2
        System.out.println((fibonacciSeries2(8))); //13
        System.out.println("==============TASK-3===============");
        Integer[] numbers1 = {};
        Integer[] numbers2 = {};
        System.out.println(Arrays.toString(findUniques(numbers1, numbers2)));
        Integer[] numbers3 = {};
        Integer[] numbers4 = {1, 2, 3, 2};
        System.out.println(Arrays.toString(findUniques(numbers3, numbers4)));
        Integer[] numbers5 = {1,2,3,4};
        Integer[] numbers6 = {3,4,5,5};
        System.out.println(Arrays.toString(findUniques(numbers5, numbers6)));
        Integer[] numbers7 = {8,9};
        Integer[] numbers8 = {9, 8, 9};
        System.out.println(Arrays.toString(findUniques(numbers7, numbers8)));
        System.out.println("==============TASK-4===============");
        System.out.println(isPowerOf3(1));
        System.out.println(isPowerOf3(3));
        System.out.println(isPowerOf3(9));
        System.out.println(isPowerOf3(27));
        System.out.println(isPowerOf3(81));
        System.out.println(isPowerOf3(0));
        System.out.println(isPowerOf3(5));


        System.out.println("==============TASK-5===============");

        System.out.println(firstDuplicate(new int[]{}));
        System.out.println(firstDuplicate(new int[]{1}));
        System.out.println(firstDuplicate(new int[]{1, 2, 2, 3}));
        System.out.println(firstDuplicate(new int[]{1, 2, 3, 4, 4}));
        System.out.println(firstDuplicate(new int[]{1, 2, 3, 2, 1, 4, 4}));

    }
    //TASK 1
    public static int[] fibonacciSeries1(int n){

        int firstNum = 0;
        int secondNum = 1;
        int nextNum;
        int[] answer = new int[n];

        for (int i = 0; i < n; i++) {
            answer[i]+= firstNum;
            nextNum = firstNum + secondNum;
            firstNum = secondNum;
            secondNum = nextNum;
        }
        return answer;
    }

    //TASK 2

    public static int fibonacciSeries2(int n){

        int firstNum = 0;
        int secondNum = 1;
        int nextNum;
        int answer = 0;

        for (int i = 0; i < n; i++) {
            answer = firstNum;
            nextNum = firstNum + secondNum;
            firstNum = secondNum;
            secondNum = nextNum;
        }
        return answer;
    }

    //TASK 3

    public static Integer[] findUniques(Integer[] numbers1, Integer[] numbers2 ){

        LinkedHashSet<Integer> list1 = new LinkedHashSet<>(Arrays.asList(numbers1));
        LinkedHashSet<Integer> list2 = new LinkedHashSet<>(Arrays.asList(numbers2));
        LinkedHashSet<Integer> answer = new LinkedHashSet<>();

        if(list1.size() == 0) return (list2).toArray(new Integer[0]);
        else if(list2.size() == 0) return (list1).toArray(new Integer[0]);

        for (Integer number : list1) {if (!list2.contains(number)) answer.add(number);}
        for (Integer number : list2) {if (!list1.contains(number) && !answer.contains(number)) answer.add(number);}

        return answer.toArray(new Integer[0]);

    }

    //TASk 4

    public static boolean isPowerOf3(int number){

        int counter = 3;
        if(number == 1) return true;
        else if( number == counter ) return true;
        else if( (counter *= 3) == number) return true;
        return false;
    }


    // TASK 5

    public static int firstDuplicate(int[] nums){
        int result = -1;
        int index = nums.length - 1;
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if(nums[i] == nums[j] && j <= index){
                    result = nums[i];
                    index = j;
                }}}
        return result;}


    }








