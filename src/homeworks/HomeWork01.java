package homeworks;

public class HomeWork01 {
    public static void main(String[] args) {
        /*
        Task-1
        JAVA -> 01001010 01000001 01010110 01000001
        SELENIUM -> 01010011 01000101 01001100 01000101 01001110 01001001 01010101 01001101
        Task-2
        MhySl
        Task-3
        following 7 source lines are the answer for Task-3.
         */


        System.out.println("-------Task-3 statements--------");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");
        System.out.println("\n");


        /*
        Task-4
        Following 1 println statement is answer for Task-4.
         */

        System.out.println("-------Task-4 Statement----------");
        System.out.println("\tJava is easy to write and easy to run—this is the foundational" +
                "\nstrength of Java and why many developers program in it. When you" +
                "\nwrite Java once, you can run it almost anywhere at any time." + "\n" + "\n" +
                "\tJava can be used to create complete applications that can run on a" +
                "\nsingle computer or be distributed across servers and clients in a" +
                "\nnetwork." + "\n" + "\n" +
                "\tAs a result, you can use it to easily build mobile applications or" +
                "\nrun-on desktop applications that use different operating systems and" +
                "\nservers, such as Linux or Windows.");

        /*
        Task-5
        Following println statements are for Task-5.
         */

        System.out.println("\n");
        System.out.println("---------Task-5 Statements----------");
        /*
        myAge -> 33
        myFavoriteNumber -> 3
        myHeight ->163
        myWeight -> 77.5
        myFavoriteCharacter -> *
         */

        byte myAge = 33;
        byte myFavoriteNumber = 3;
        short myHeight = 163;
        float myWeight = 77.5f;
        char myFavoriteCharacter = '*';

        System.out.println(myAge); // 33
        System.out.println(myFavoriteNumber); // 3
        System.out.println(myHeight); // 163
        System.out.println(myWeight); // 77.5
        System.out.println(myFavoriteCharacter); // *




    }
}
