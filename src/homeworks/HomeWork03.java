package homeworks;

import java.util.Scanner;

public class HomeWork03 {
    public static void main(String[] args) {

        System.out.println("\n------------------------TASK1-------------------------\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, can you give me a number!");
        int num1 = input.nextInt();

        System.out.println("Okay, now can you give me another number!");
        int num2 = input.nextInt();
        input.nextLine();

        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));

        System.out.println("\n------------------------TASK2-------------------------\n");

        System.out.println("Can you give me a number!");
        int number1 = input.nextInt();

        System.out.println("Can you give me another number!");
        int number2 = input.nextInt();

        System.out.println("Ok, can you give me another number!");
        int number3 = input.nextInt();

        System.out.println("Ok, can you give me another number!");
        int number4 = input.nextInt();

        System.out.println("Thanks, can you give me one last number!");
        int number5 = input.nextInt();
        input.nextLine();


        System.out.println("Max value is = " + Math.max (Math.max(Math.max(number1 , number2) ,
                Math.max(number3 , number4)) , number5 ));
        System.out.println("Min value is = " + Math.min (Math.min(Math.min(number1 , number2) ,
                Math.min(number3 , number4)) , number5 ));


        System.out.println("\n------------------------TASK3-------------------------\n");

        int randomNumber1 = (int) (Math.random() * 51) + 50;
        int randomNumber2 = (int) (Math.random() * 51) + 50;
        int randomNumber3 = (int) (Math.random() * 51) + 50;

        System.out.println("Number 1 = " + randomNumber1);
        System.out.println("Number 2 = " + randomNumber2);
        System.out.println("Number 3 = " + randomNumber3);
        System.out.println("The sum of numbers is = " + (randomNumber1 + randomNumber2 + randomNumber3));



        System.out.println("\n------------------------TASK4-------------------------\n");

        double alex = 125, mike = 220;

        System.out.println("Alex's money: $" + (alex -= 25.5));
        System.out.println("Mike's money: " + (mike += 25.5));



        System.out.println("\n------------------------TASK5-------------------------\n");

        double bicycle = 390;
        double savingsPerDay = 15.60;

        System.out.println((int)(bicycle / savingsPerDay));



        System.out.println("\n------------------------TASK6-------------------------\n");

        String s1 = "5", s2 = "10";
        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);
        input.nextLine();

        System.out.println("Sum of 5 and 10 is = " + (i1 + i2));
        System.out.println("Product of 5 and 10 is = " + (i1 * i2));
        System.out.println("Division of 5 and 10 is = "+ (i1 / i2));
        System.out.println("Subtraction of 5 and 10 is = " + (i1 - i2));
        System.out.println("Remainder of 5 and 10 is = " + (i1 % i2));



        System.out.println("\n------------------------TASK7-------------------------\n");


        String string1 = "200", string2 = "-50";

        int value1 = Integer.parseInt(string1), value2 = Integer.parseInt(string2);

        System.out.println("The greatest value is = " + Math.max(value1 , value2));
        System.out.println("The smallest value is = " + Math.min(value1 , value2));
        System.out.println("The average is = " + ((value1 + value2) / 2));
        System.out.println("The absolute difference is = " + Math.abs(value1 - value2));



        System.out.println("\n------------------------TASK8-------------------------\n");

        /*
        3 quarters = 75 cents
        1 dime = 10 cents
        2 nickels = 10 cents
        1 penny = 1 cent
        My saving per day = $0.96
         */

        double myDailySaving = 0.96;

        System.out.println((int)(24 / myDailySaving) + " days");
        System.out.println((int)(168 / myDailySaving) + " days");
        System.out.println("$" + (myDailySaving * (5 * 30)));



        System.out.println("\n------------------------TASK9-------------------------\n");

        double dailySaving = 62.5;
        double computer = 1250;

        System.out.println((int)(computer / dailySaving));

        System.out.println("\n------------------------TASK10-------------------------\n");

        double carValue = 14265;
        double paymentOption1 = 475.50;
        double paymentOption2 = 951;

        System.out.println("Option 1 will take " + (int)(carValue / paymentOption1) + " months");
        System.out.println("Option 2 will take " + (int)(carValue / paymentOption2) + " months");



        System.out.println("\n------------------------TASK11-------------------------\n");


        System.out.println("Hey user can you give me a number?");
        int myNumber1 = input.nextInt(), myNumber2 = input.nextInt();

        System.out.println(((double)myNumber1 / (double)myNumber2));



        System.out.println("\n------------------------TASK12-------------------------\n");

        int myRandomNumber1 = (int)(Math.random() * 101),  myRandomNumber2 = (int)(Math.random() * 101),
        myRandomNumber3 = (int)(Math.random() * 101);

        System.out.println(myRandomNumber1);
        System.out.println(myRandomNumber2);
        System.out.println(myRandomNumber3);

        if(myRandomNumber1 > 25){
            System.out.println("true");
        }
        else if(myRandomNumber2 > 25){
            System.out.println("true");
        }
        else if(myRandomNumber3 > 25){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }

        System.out.println("\n------------------------TASK13-------------------------\n");


        System.out.println("Can you enter a number between 1 and 7, both numbers included please!");
        int luckyNumber = input.nextInt();
        input.nextLine();

        if (luckyNumber == 1){
            System.out.println("The number entered returns MONDAY");
        }
        else if (luckyNumber == 2){
            System.out.println("The number entered returns TUESDAY");
        }
        else if (luckyNumber == 3){
            System.out.println("The number entered returns WEDNESDAY");
        }
        else if (luckyNumber == 4){
            System.out.println("The number entered returns THURSDAY");

        }else if (luckyNumber == 5){
            System.out.println("The number entered returns FRIDAY");
        }
        else if (luckyNumber == 6){
            System.out.println("The number entered returns SATURDAY");
        }
        else{
            System.out.println("The number entered returns SUNDAY");

        }


        System.out.println("\n------------------------TASK14-------------------------\n");

        System.out.println("Jennifer, could you enter your exam results!");
        int examResult1 = input.nextInt(), examResult2 = input.nextInt(), examResult3 = input.nextInt();
        input.nextLine();


        int averageOfExamResults = (examResult1 + examResult2 + examResult3) / 3;

        if(averageOfExamResults >= 70){
            System.out.println("YOU PASSED!");
        }
        else{
            System.out.println("YOU FAILED!");
        }



        System.out.println("\n------------------------TASK15-------------------------\n");

        System.out.println("Hey user can you enter three numbers?");
        int userNumber1 = input.nextInt(), userNumber2 = input.nextInt(), userNumber3 = input.nextInt();

        if ((userNumber1 == userNumber2) && (userNumber1 == userNumber3)) {
            System.out.println("TRIPLE MATCH");
            }

        else if((userNumber1 == userNumber2) || (userNumber2 == userNumber3)){
            System.out.println("DOUBLE MATCH");
        }
        else{
            System.out.println("NO MATCH");
        }

    }

}
