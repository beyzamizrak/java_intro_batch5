package homeworks;

import org.omg.CORBA.MARSHAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Homework11 {


    //TASK 1
    public static String noSpace(String str){

       return str.replaceAll(" ", "");
    }
    //TASK 2
    public static String replaceFirstLast(String str){
        if (str.length() < 2) return "";
        else{
            str.trim();
            String strNew = str.substring(str.length()-1)+ str.substring(1, str.length()-1) + str.substring(0,1);
            return strNew;
        }
    }

    //TASK 3
    public static boolean hasVowel(String str){

        if (str.contains("a") || str.contains("e")||  str.contains("i") || str.contains("o") ||
                str.contains("u") || str.contains("A") || str.contains("E")||  str.contains("I") ||
                str.contains("O") || str.contains("U")) return true;
        return false;
    }

    //TASK 4

    public static void checkAge(int yearOfBirth){

        if ( 2022 - yearOfBirth < 16) System.out.println("AGE IS NOT ALLOWED");
        else if (2022 - yearOfBirth >= 16 && 2022 - yearOfBirth < 100) System.out.println("AGE IS ALLOWED");
        else System.out.println("AGE IS NOT VALID");

    }

    // TASK 5

    public static int averageOfEdges(int a, int b, int c){

        int max = Math.max(Math.max(a,b), c);
        int min = Math.min(Math.min(a,b), c);

        return (min + max) / 2;

    }

    //TASK 6

    public static String[] noA(String[] words){

        for (int i = 0; i < words.length; i++) {
            if(words[i].toLowerCase().startsWith("a")) words[i] = "###";
        }
        return words;
    }

    //TASK 7

    public static int[] no3or5(int[] numbers){

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 15 == 0) numbers[i] = 101;
            else if (numbers[i] % 5 == 0) numbers[i] = 99;
            else if (numbers[i] % 3 == 0) numbers[i] = 100;
        }

        return numbers;
    }

    //TASK 8
    public static int countPrimes(int [] arr8){

        int counter = 0;
        for (int num : arr8) {
            if(num == 2 && num == 3) counter++;
            else if (num > 3) {
                boolean prime = true;
                for (int j = 2; j < num ; j++) {
                    if(num % j == 0){
                        prime = false;
                    }
                }
                if(prime) counter++;
            }
        }
        return counter;
    }




    public static void main(String[] args) {

        System.out.println("\n-------------------------TASK 1-----------------------\n");

        String str = " Hello World   ";

        System.out.println(noSpace(str));

        System.out.println("\n-------------------------TASK 2-----------------------\n");

        String str1 = "Tech Global";

        System.out.println(replaceFirstLast(str1));

        System.out.println("\n-------------------------TASK 3-----------------------\n");

        String str2 = "Allll";

        System.out.println(hasVowel(str2));

        System.out.println("\n-------------------------TASK 4-----------------------\n");

        int year = 2006;

        checkAge(year);

        System.out.println("\n-------------------------TASK 5-----------------------\n");

        System.out.println(averageOfEdges(0, 0, 6));

        System.out.println("\n-------------------------TASK 6-----------------------\n");

        String[] words = {"java", "anna", "123", "xyz"};

        System.out.println(Arrays.toString(noA(words)));

        System.out.println("\n-------------------------TASK 7-----------------------\n");

        int[] numbers = {3, 4, 5, 6};

        System.out.println(Arrays.toString(no3or5(numbers)));

        System.out.println("\n-------------------------TASK 8-----------------------\n");

        int[] numbers1 = {7, 4, 11, 23, 17};

        System.out.println(countPrimes(numbers1));

    }
}
