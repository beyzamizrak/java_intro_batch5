package homeworks;

import java.util.Arrays;

public class HomeWork06 {
    public static void main(String[] args) {

        System.out.println("\n------------------------------------TASK_1------------------------------------\n");

        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n------------------------------------TASK_2------------------------------------\n");


        String[] words = new String[5];

        words[1] = "abc";
        words[4] = "xyz";

        System.out.println(words[3]);
        System.out.println(words[0]);
        System.out.println(words[4]);
        System.out.println(Arrays.toString(words));

        System.out.println("\n------------------------------------TASK_3------------------------------------\n");

        int[] numbers1 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers1));

        Arrays.sort(numbers1);

        System.out.println(Arrays.toString(numbers1));

        System.out.println("\n------------------------------------TASK_4------------------------------------\n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        System.out.println("\n------------------------------------TASK_5------------------------------------\n");


        String[] cartoonDogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoonDogs));

        boolean hasPluto = false;
        for (String cartoonDog : cartoonDogs) {
            if(cartoonDog.equals("Pluto")) { hasPluto = true;
                break;
            }

        }
        System.out.println(hasPluto);

        System.out.println("\n------------------------------------TASK_6------------------------------------\n");


        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cartoonCats);
        System.out.println(Arrays.toString(cartoonCats));

        boolean hasGarfieldAndFelix = false;

        for (String cartoonCat : cartoonCats) {
            if(cartoonCat.equals("Garfield") && cartoonCat.equals("Felix")){
                hasGarfieldAndFelix = true;
                break;
            }

        }
        System.out.println(hasGarfieldAndFelix);

        System.out.println("\n------------------------------------TASK_7------------------------------------\n");

        double[] doubleNumbers = {10.5, 20.75, 70.0, 80.0, 15.75};

        System.out.println(Arrays.toString(doubleNumbers));

        for (double doubleNumber : doubleNumbers) {
            System.out.println(doubleNumber);
        }
        System.out.println("\n------------------------------------TASK_8------------------------------------\n");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        int countLetters = 0;
        int countUpperCase = 0;
        int countLowerCase = 0;
        int countDigits = 0;
        int countSpecials = 0;

        for (char character : characters) {
            if(Character.isLetter(character)) {
                countLetters++;
                if (Character.isUpperCase(character)) countUpperCase++;
                else countLowerCase++;
            }
            else if(Character.isDigit(character)) countDigits++;
            else countSpecials++;
        }

        System.out.println("Letters = " + countLetters);
        System.out.println("Uppercase letters = " + countUpperCase);
        System.out.println("Lowercase letters = " + countLowerCase);
        System.out.println("Digits = " + countDigits);
        System.out.println("Special characters = " + countSpecials);

        System.out.println("\n------------------------------------TASK_9------------------------------------\n");

        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects));

        int countUpCase = 0;
        int countLowCase = 0;
        int countBOrP = 0;
        int countBookOrPen = 0;

        for (String object : objects) {
            if (Character.isUpperCase(object.charAt(0))) {
                countUpCase++;
            } else if (Character.isLowerCase(object.charAt(0))) {
                countLowCase++;
            }
            if ((object.toUpperCase().charAt(0) == 'B') ||(object.toUpperCase().charAt(0) == 'P') ) {
                countBOrP++;
            }if (object.toLowerCase().contains("book") || object.toLowerCase().contains("pen")) {
                countBookOrPen++;
            }
        }

        System.out.println("Elements starts with uppercase = " + countUpCase);
        System.out.println("Elements starts with lowercase = " + countLowCase);
        System.out.println("Elements starting with B or P = " + countBOrP);
        System.out.println("Elements having ”book” or “pen” = " + countBookOrPen);

        System.out.println("\n------------------------------------TASK_10------------------------------------\n");

        int[] myNumbers = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        System.out.println(Arrays.toString(myNumbers));

        int moreThan10 = 0;
        int lessThan10 = 0;
        int ten = 0;

        for (int myNumber : myNumbers) {
            if(myNumber > 10) {
                moreThan10++;
            }
            else if (myNumber < 10){
                lessThan10++;
            }
            else if(myNumber == 10) {
                ten++;
            }
        }

        System.out.println("Elements that are more than 10 = " + moreThan10);
        System.out.println("Elements that are less than 10 = " + lessThan10);
        System.out.println("Elements that are 10 = " + ten);

        System.out.println("\n------------------------------------TASK_11------------------------------------\n");

        int[] first = {5, 8, 13, 1, 2};
        int[] second = {9, 3, 67, 1, 0};
        int[] third = new int[5];

        for (int i = 0; i < third.length; i++) {
            third[i] = Math.max(first[i], second[i]);
        }

        System.out.println(Arrays.toString(third));

        System.out.println("Testing git lab");
        System.out.println("test test");
        
    }
}
