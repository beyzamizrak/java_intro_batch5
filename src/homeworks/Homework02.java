package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("-------------------------TASK1-------------------------\n");

        int data = 5, data2 = 10;

        System.out.print("Hey user, can you give me one number!" + "\n");
        data = input.nextInt();

        System.out.print("The number 1 entered by the user is = " + (data) + "\n");

        System.out.print("Now, can you give me another number!" + "\n");
        data2 = input.nextInt();

        System.out.print("The number 2 entered by the user is = " + (data2) + "\n");

        input.nextLine();

        System.out.print("The sum of number 1 and 2 entered by the user is = " + (data + data2) + "\n");

        System.out.println();

        System.out.println("-------------------------TASK2-------------------------\n");

        int firstNumber = 3;
        int secondNumber = 5;

        System.out.print("Hey user, can you give me a number!" + "\n");
        firstNumber = input.nextInt();

        System.out.print("Okay now, can you give me second number!" + "\n");
        secondNumber = input.nextInt();

        System.out.print("The product of the given 2 numbers is: " + (firstNumber * secondNumber) + "\n");

        System.out.println();

        System.out.println("-------------------------TASK3-------------------------\n");

        double num1 = 24;
        double num2 = 10;

        System.out.print("Hey user, can you give me a number please!" + "\n");
        num1 = input.nextInt();

        System.out.print("thank you, now, can you give me another number" + "\n");
        num2 = input.nextInt();

        input.nextLine();

        System.out.print("The sum of the given numbers is = " + (num1 + num2) + "\n");
        System.out.print("The product of the given numbers is = " + (num1 * num2)+ "\n");
        System.out.print("The subtraction of the given numbers is = " + (num1 - num2)+ "\n");
        System.out.print("The division of the given numbers is = " + (num1 / num2)+ "\n");
        System.out.print("The remainder of the given numbers is = " + (num1 % num2) + "\n");

        System.out.println();

        System.out.println("-------------------------TASK4-------------------------\n");

        int a = -10, b = 7, c = 5, d = 72, e = 24, f = -3, g = 9, h = 3, i = 18, j = 6, k = 10;
        int problem1 = a + b * c;
        int problem2 = (d + e) % e;
        int problem3 = k + f * g / g;
        int problem4 = c + i / h * h - (j % h);

        System.out.print(a + b * c + "\n");
        System.out.print((d + e) % e + "\n");
        System.out.print(k + f * g / g + "\n");
        System.out.print(c + i / h * h - (j % h) + "\n");

        System.out.println();

        System.out.println("-------------------------TASK5-------------------------\n");

        int digit1 = 7, digit2 = 11;

        System.out.print("Hey user can you write down a number!" + "\n");
        digit1 = input.nextInt();

        System.out.print("Okay, can you write down another number!" + "\n");
        digit2 = input.nextInt();

        System.out.println("The average of the given numbers is: " + ((digit1 + digit2) / 2) + "\n");


        System.out.println("-------------------------TASK6-------------------------\n");

        int number1 = 6, number2 = 10, number3 = 12, number4 = 15, number5 = 17;

        System.out.print("Hey user! Can you enter a number! " + "\n");
        number1 = input.nextInt();

        System.out.print("Thanks! Can you enter another number! " + "\n");
        number2 = input.nextInt();

        System.out.print("Okay, now can you enter another number! " + "\n");
        number3 = input.nextInt();

        System.out.print("Then, can you enter another number! " + "\n");
        number4 = input.nextInt();

        System.out.print("Finally, can you enter another number! " + "\n");
        number5 = input.nextInt();

        System.out.print("The average of the given numbers is: " +
                ((number1 + number2 + number3 + number4 + number5) / 5) + "\n");


        System.out.println("\n-------------------------TASK7-------------------------\n");

        int square1 = 5, square2 = 6, square3 = 10;

        System.out.print("Hey user, can you enter a number for me!" + "\n");
        square1 = input.nextInt();

        System.out.print("Ok, can you enter another number now!" + "\n");
        square2 = input.nextInt();

        System.out.print("Thanks, can you enter one last number!" + "\n");
        square3 = input.nextInt();

        System.out.print("The 5 multiplied with 5 is = " + (square1 * square1) + "\n");

        System.out.print("The 6 multiplied with 6 is = " + (square2 * square2) + "\n");

        System.out.print("The 10 multiplied with 10 is = " + (square3 * square3) + "\n");


        System.out.println("\n-------------------------TASK8-------------------------\n");

        int side = 7;

        System.out.println("Hey user, what is the side of the square that you have?");
        side = input.nextInt();

        System.out.print("Perimeter of the square = " + (4 * side) + "\n");
        System.out.print("Area of the square is = " + (side * side) + "\n");

        System.out.println("\n-------------------------TASK9-------------------------\n");

        double averageIncome = 90000;

        System.out.print("A Software Engineer in Test can earn $" + (averageIncome * 3) + " in 3 years." + "\n");


        System.out.println("\n-------------------------TASK10-------------------------\n");

        input.nextLine();

        String favBook = "Java 101", favColor = "blue";
        int favNumber = 7;

        System.out.print("Hey user, what is your favorite book? " + "\n");
        favBook = input.nextLine();

        System.out.print("Hey user, what is your favorite color? " + "\n");
        favColor = input.nextLine();

        System.out.print("Hey user, what is your favorite number? " + "\n");
        favNumber = input.nextInt();

        System.out.print("User’s favorite book is: " + favBook + "\n" +
                "User’s favorite color is: " +  favColor + "\n" +
                "User’s favorite number is: " + favNumber + "\n");



        System.out.println("\n-------------------------TASK11-------------------------\n");


        String firstName, lastName, emailAddress, phoneNumber, address;
        int age = 45;

        input.nextLine();

        System.out.print("Hey user! What is your name?" + "\n");
        firstName = input.nextLine();

        System.out.print("Hey user! What is your last name?" + "\n");
        lastName = input.nextLine();

        System.out.print("What is your age? " + "\n");
        age = input.nextInt();

        input.nextLine();

        System.out.print("What is your email address? " + "\n");
        emailAddress = input.nextLine();

        System.out.print("Okay, can you tell me your phone number?" + "\n");
        phoneNumber = input.nextLine();

        System.out.print("What is your address? " + "\n");
        address = input.nextLine();

        System.out.print("\tUser who joined this program is " + firstName + " " + lastName +
                "." + " " + firstName + "’s\n" +
                " age is " + age + "." + " " + firstName + "’s" + " email\n" + "address is " +
                emailAddress + "," + "phone number is " + " " + phoneNumber + "," +
                " and address is\n" + address + "." + "\n");
    }
}
