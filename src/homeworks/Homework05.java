package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n------------------------------------TASK_1------------------------------------\n");

        String result1 = "";

        for (int i = 1; i <= 100 ; i++) {
            if ( i % 7 == 0 ) result1 += i + " - ";
        }

        System.out.println(result1.substring(0, result1.length() -3));

        System.out.println("\n------------------------------------TASK_2------------------------------------\n");

        String result2 = "";

        for (int i1 = 1; i1 <= 50 ; i1++) {
            if (i1 % 6 == 0) result2 += i1 + " - ";

        }

        System.out.println(result2.substring(0, result2.length() - 3));

        System.out.println("\n------------------------------------TASK_3------------------------------------\n");

        String result3 = "";

        for (int i3 = 100; i3 >=50 ; i3--) {
            if (i3 % 5 == 0) result3 += i3 + " - ";

        }

        System.out.println(result3.substring(0, result3.length() - 3));

        System.out.println("\n------------------------------------TASK_4------------------------------------\n");

        for (int i4 = 0; i4 <= 7 ; i4++) {
            System.out.println("The square of " + i4 + " is = " + i4 * i4);
        }

        System.out.println("\n------------------------------------TASK_5------------------------------------\n");

        int sum1 = 0;

        for (int i5 = 1; i5 <= 10 ; i5++) {
            sum1 += i5;

        }
        System.out.println(sum1);

        System.out.println("\n------------------------------------TASK_6------------------------------------\n");

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a positive number");
        int positiveNumber = scanner.nextInt();
        scanner.nextLine();

        int container1 = 1;

        for (int i6 = 1; i6 <= positiveNumber ; i6++) {
            container1 *= i6;

        }
        System.out.println(container1);

        System.out.println("\n------------------------------------TASK_7------------------------------------\n");

        System.out.println("Can you enter your full name?");
        String fullName = scanner.nextLine();

        int counter1 = 0;

        for (int i7 = 0; i7 < fullName.length(); i7++) {
            if(fullName.toLowerCase().charAt(i7) == 'a' || fullName.toLowerCase().charAt(i7) == 'e' ||
                    fullName.toLowerCase().charAt(i7) == 'i' || fullName.toLowerCase().charAt(i7) == 'o' ||
                    fullName.toLowerCase().charAt(i7) == 'u')
                    counter1++;
        }
        System.out.println("There are " + counter1 + " vowel letters in this full name");

        System.out.println("\n------------------------------------TASK_08------------------------------------\n");

        int sum = 0;
        int specialNumber = 0;

        do{
            sum+= ScannerHelper.getANumber();
            specialNumber++;
        }
        while(sum < 100);
            if (specialNumber == 1) System.out.println("This number is already more than 100");
            else System.out.println("Sum of the entered numbers is at least 100");


        System.out.println("\n------------------------------------TASK_09------------------------------------\n");

        int givenNum = 7;
        int firstNum = 0;
        int secondNum = 1;
        int nextNum;

        String answer = "";

            for (int j = 0; j < givenNum; j++) {
                answer += firstNum + " - ";
                nextNum = firstNum + secondNum;
                firstNum = secondNum;
                secondNum = nextNum;
            }

            System.out.println(answer.substring(0, answer.length() -3));


        System.out.println("\n------------------------------------TASK_10------------------------------------\n");


        String name;

        do{
            name = ScannerHelper.getAName();
        }
        while(name.toLowerCase().charAt(0) != 'j');
        System.out.println("End of the program");






















    }
}
